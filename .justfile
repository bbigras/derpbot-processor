build-all:
  nix build .#docker
  nix build .#packages.aarch64-linux.docker

push:
  nix run .#docker.copyToRegistry

docker:
  # nix run .#docker.copyTo docker-daemon:derpbot-processor:amd64
  # nix run .#packages.aarch64-linux.docker.copyTo docker-daemon:derpbot-processor:arm64
  nix run .#docker.copyTo docker://registry.gitlab.com/bbigras/derpbot4:amd64
  nix run .#packages.aarch64-linux.docker.copyTo docker://registry.gitlab.com/bbigras/derpbot4:arm64

aarch64:
  nix run .#packages.aarch64-linux.docker.copyTo docker://registry.gitlab.com/bbigras/derpbot-processor:aarch64
  
# docker manifest create registry.gitlab.com/bbigras/derpbot4:latest \
#   derpbot-processor:amd64 \
#   derpbot-processor:arm64

#docker manifest create registry.gitlab.com/bbigras/derpbot4:processor \
#  registry.gitlab.com/bbigras/derpbot4:amd64 \
#  registry.gitlab.com/bbigras/derpbot4:arm64


# docker manifest push your-repo/your-image:latest

regen:
  graphql-client introspect-schema http://127.0.0.1:4000 > schema.json
  graphql-client generate queries.graphql --schema-path schema.json --custom-scalars-module crate::custom_scalars -o src --response-derives=Clone,Debug --variables-derives=Debug
