{ pkgs
, generatedBuild ? ./Cargo.nix
, rustc
}:
let
  customBuildRustCrateForPkgs = pkgs: pkgs.buildRustCrate.override {
    inherit rustc;
    defaultCrateOverrides = pkgs.defaultCrateOverrides // {
      opentelemetry-proto = _attrs: with pkgs; {
        nativeBuildInputs = with pkgs; [
          protobuf
        ];
      };
    };
  };
  basePackage = pkgs.callPackage generatedBuild {
    inherit pkgs;
    buildRustCrateForPkgs = customBuildRustCrateForPkgs;
  };
  submodulePackage = basePackage.rootCrate.build;
in
submodulePackage
