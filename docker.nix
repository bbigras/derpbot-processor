{ pkgs, myCrate, nix2container }:

nix2container.buildImage {
  name = "registry.gitlab.com/bbigras/derpbot-processor";
  tag = "latest";

  maxLayers = 100;

  contents = with pkgs; [
    coreutils
    dockerTools.binSh
    dockerTools.caCertificates
    fakeNss
    openssl
  ];

  config = {
    # User = "65534:65534"; # nobody
    # WorkingDir = "/web-app";
    entrypoint = [ "${myCrate}/bin/derpbot-processor" ];
  };
}
