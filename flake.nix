{
  description = "derpbot-processor";

  nixConfig = {
    extra-substituters = [
      "https://nix-community.cachix.org"
      "https://pre-commit-hooks.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "pre-commit-hooks.cachix.org-1:Pkk3Panw5AW24TOv6kz3PvLhlH8puAsJTBbOPmBo7Rc="
    ];
  };

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    fenix.url = "github:nix-community/fenix";
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix2container = {
      url = "github:nlewo/nix2container";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    nix-filter2.url = "github:numtide/nix-filter";
  };

  outputs = { self, nixpkgs, flake-utils, fenix, pre-commit-hooks, crane, nix2container, nix-filter2, ... }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          nix-filter = import nix-filter2;

          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
            overlays = [
              fenix.overlays.default
            ];
          };

          myRust = pkgs.fenix.complete.withComponents [
            "cargo"
            "clippy"
            "rust-src"
            "rustc"
            "rustfmt"
            "rust-analyzer"
          ];

          craneLib = crane.lib.${system}.overrideToolchain
            fenix.packages.${system}.minimal.toolchain;

          # Common derivation arguments used for all builds
          commonArgs = {
            # src = craneLib.cleanCargoSource ./.;
            src = nix-filter {
              root = ./.;
              include = [
                "askama.toml"
                "Cargo.toml"
                "Cargo.lock"
                "schema.json"
                "schema-avro.json"
                "schema2.json"
                "src"
                "templates"
                # "sqlx-data.json"
              ];
            };

            buildInputs = with pkgs; [
              # Add extra build inputs here, etc.
              openssl
            ];

            nativeBuildInputs = with pkgs; [
              # Add extra native build inputs here, etc.
              # capnproto
              pkg-config
              # protobuf
            ];

            doCheck = false;
          };

          # Build *just* the cargo dependencies, so we can reuse
          # all of that work (e.g. via cachix) when running in CI
          cargoArtifacts = craneLib.buildDepsOnly (commonArgs // {
            # Additional arguments specific to this derivation can be added here.
            # Be warned that using `//` will not do a deep copy of nested
            # structures
            pname = "derpbot-processor";
          });

          # Build the actual crate itself, reusing the dependency
          # artifacts from above.
          myCrate = craneLib.buildPackage (commonArgs // {
            inherit cargoArtifacts;
          });

          # inherit (fenix.packages.${system}.minimal) rustc;

          # my-crate = import ./default.nix { inherit pkgs rustc; };

          docker = import ./docker.nix {
            inherit myCrate pkgs;
            inherit (nix2container.packages."${system}") nix2container;
            # myCrate = my-crate;
          };
        in
        {
          devShells.default = import ./shell.nix { inherit pkgs pre-commit-hooks system myRust; };
          packages = {
            inherit docker;
            default = myCrate;
          };
        }
      );
}
