use anyhow::Error;
use async_nats::jetstream::consumer::DeliverPolicy;
use async_nats::jetstream::{context::Context as JsContext, Message};
use futures_util::StreamExt;
use reqwest_middleware::ClientWithMiddleware;
use secrecy::SecretString;
use serde::{Deserialize, Serialize};
use serde_avro_fast::{from_single_object_slice, to_single_object, Schema};
use tracing::{error, info, instrument, trace};

use crate::discord;

const SUBJECT: &str = "discord_posts";

#[derive(Debug, Default, Deserialize, Serialize)]
struct Snapshot {
    stream_sequence: u64,
    list: Vec<DiscordServerPost>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq)]
pub enum DiscordMsgType {
    Request,
    Reply,
    #[default]
    Unknown,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct DiscordServerPost {
    pub command: String,
    pub msg_id: String,
    pub channel_id: String,
    pub msg_type: DiscordMsgType,
}

#[instrument(err, skip(nats_client))]
pub async fn post_nats_sp(
    nats_client: &async_nats::Client,
    server_post: DiscordServerPost,
) -> Result<(), Error> {
    trace!("post_nats_sp");

    let schema: Schema = include_str!("../schema-avro.json").parse().unwrap();

    use serde_avro_fast::ser::SerializerConfig;
    use serde_avro_fast::{to_single_object, Schema};

    let config = &mut SerializerConfig::new(&schema);

    let writer = vec![];

    let q = to_single_object(&server_post, writer, config).unwrap();
    // dbg!(&q);

    nats_client.publish(SUBJECT, q.into()).await?;

    Ok(())
}

#[tracing::instrument(skip_all)]
pub async fn f_server2(
    // nats_client: async_nats::Client,
    jetstream: JsContext,
    http_client: ClientWithMiddleware,
    token: SecretString,
) -> Result<(), Error> {
    let kv_snapshots = jetstream.get_key_value("snapshots").await?;

    let snapshot: Snapshot = kv_snapshots
        .get("track-discord-posts")
        .await?
        .and_then(|v| {
            let schema2: Schema = include_str!("../schema2.json").parse().unwrap();
            match from_single_object_slice(&v, &schema2) {
                Ok(s) => Some(s),
                Err(e) => {
                    error!("from_single_object_slice: {:?}", e);
                    None
                }
            }
        })
        .flatten()
        .unwrap_or_default();
    // dbg!(&snapshot);
    // todo!();

    let stream_sequence = snapshot.stream_sequence;
    let mut start_sequence = stream_sequence + 1;

    info!("start_sequence = {}", start_sequence);

    // FIXME
    if start_sequence == 1 {
        start_sequence = 0;
    }

    let schema: Schema = include_str!("../schema-avro.json").parse().unwrap();

    let mut list: Vec<DiscordServerPost> = snapshot.list;

    let stream = jetstream.get_stream(SUBJECT).await?;

    let mut consumer_config = async_nats::jetstream::consumer::pull::OrderedConfig {
        name: None,
        // deliver_policy: DeliverPolicy::ByStartSequence { start_sequence },
        ..Default::default()
    };

    if start_sequence > 0 {
        consumer_config.deliver_policy = DeliverPolicy::ByStartSequence { start_sequence };
    }

    match stream.create_consumer(consumer_config).await {
        Ok(mut consumert) => {
            // message.ack().await.unwrap();

            // let num_pending = {
            //     let info = consumert.info().await?;
            //     info.num_pending
            // };

            let mut messages = consumert.messages().await?;
            loop {
                // let mut messages = msgs.take(100);

                // let mut messages = consumer.fetch().max_messages(200).messages().await.unwrap();
                let mut i = 0;
                while let Some(Ok(message)) = messages.next().await {
                    let info = message.info().unwrap();
                    // println!("seq={}", info.stream_sequence);

                    // let payload = String::from_utf8_lossy(&message.message.payload);

                    let q: DiscordServerPost =
                        // from_single_object_slice(&message.message.payload, &schema).unwrap();
                        match from_single_object_slice(&message.message.payload, &schema) {
                            Ok(s) => s,
                            Err(e) => {
                                error!("from_single_object_slice: {:?}", e);
                                continue;
                            }
                        };
                    // dbg!(&q);

                    for one in list
                        //.iter_mut()
                        .extract_if(|v: &mut DiscordServerPost| {
                            v.channel_id == q.channel_id
                                && v.msg_type == q.msg_type
                                && v.command == q.command
                        })
                    {
                        let channel_id = one.channel_id;
                        let message_id = one.msg_id;
                        if let Err(e) =
                            discord::delete_message(&http_client, &channel_id, &message_id, &token)
                                .await
                        {
                            error!("discord::delete_message: {:?}", e);
                        }
                    }

                    list.push(q);

                    info!("snapshot, seq={}", info.stream_sequence);
                    let snap = Snapshot {
                        stream_sequence: info.stream_sequence,
                        // servers: servers.clone(),
                        list: list.clone(),
                    };
                    // let value = serde_json::to_string(&snap).unwrap();

                    let schema: Schema = include_str!("../schema2.json").parse().unwrap();

                    use serde_avro_fast::ser::SerializerConfig;
                    use serde_avro_fast::{to_single_object, Schema};

                    let config = &mut SerializerConfig::new(&schema);

                    let writer = vec![];

                    let value = to_single_object(&snap, writer, config).unwrap();
                    kv_snapshots
                        .put("track-discord-posts", value.into())
                        .await?;

                    message.ack().await.unwrap();
                }
            }
        }
        Err(e) => {
            error!("error: {:?}", e);
            use async_nats::jetstream::stream::ConsumerErrorKind;
            match e.kind() {
                ConsumerErrorKind::JetStream(e2) => match e2.error_code() {
                    async_nats::jetstream::ErrorCode::MAXIMUM_CONSUMERS_LIMIT => {}
                    _ => {
                        error!("error: {:?}", e);
                    }
                },
                _ => {
                    error!("error: {:?}", e);
                }
            }
        }
    }

    Ok(())
}

#[test]
fn test_encode() -> Result<(), Error> {
    let schema: Schema = include_str!("../schema-avro.json")
        .parse()
        .context("can't parse schema")?;

    use serde_avro_fast::ser::SerializerConfig;
    use serde_avro_fast::{to_single_object, Schema};

    let config = &mut SerializerConfig::new(&schema);

    let writer = vec![];

    let server_post = DiscordServerPost {
        command: "test".to_string(),
        msg_id: "123".to_string(),
        channel_id: "456".to_string(),
        msg_type: DiscordMsgType::Request,
    };

    let q = to_single_object(&server_post, writer, config).unwrap();
    dbg!(&q);

    Ok(())
}
