use std::net::SocketAddr;
use std::time::Duration;

use anyhow::{bail, Error};
use async_nats::jetstream;
use tokio::net::UdpSocket;
use tokio::time::timeout;
use tracing::{error, trace};

use crate::queries::get_server::GetServerServerOnSquadServer;
use crate::queries::get_server_with_players::{
    GetServerWithPlayersServer, GetServerWithPlayersServerOnSquad44Server,
    GetServerWithPlayersServerOnSquadServer,
};
use crate::steam::query::{Fetcher, MySocket};
use crate::{PlayerName, UserVisibleError};

const TIMEOUT_MS: u64 = 10_000;

#[tracing::instrument]
async fn server_one(addr: SocketAddr) -> Result<u8, Error> {
    trace!("server_one");

    let socket = UdpSocket::bind("0.0.0.0:0").await?;

    let socket = Box::new(MySocket { socket });
    let mut f = Fetcher { socket };
    let r = timeout(
        Duration::from_millis(TIMEOUT_MS),
        // f.get_number_players(addr),
        f.ping(addr),
    )
    .await
    .map_err(|_e: tokio::time::error::Elapsed| UserVisibleError::Timeout)??;

    dbg!(&r);
    todo!();

    /*
    Ok(r)
        */
}

#[tracing::instrument]
pub async fn server_old() -> Result<String, Error> {
    trace!("server_old");

    let ip: std::net::IpAddr = "92.118.18.108".parse()?;

    let mut reply = String::new();

    let binding = hostname::get().unwrap();
    let hostname_real = binding.to_str().unwrap();

    let hostname = std::env::var("HOSTNAME").unwrap_or_else(|_| hostname_real.to_string());
    let host_info = format!(
        "{} {} {} {}",
        hostname,
        std::env::consts::ARCH,
        std::env::consts::FAMILY,
        std::env::consts::OS
    );

    // TODO: concurrent
    for (name, addr) in vec![
        ("tpg1", (ip, 27165).into()),
        // ("tpg2", (ip, 28165).into())
    ] {
        match server_one(addr).await {
            Ok(r) => {
                reply.push_str(&format!("**{}**: {} players\n", name, r));
            }
            Err(e) => match e.downcast_ref::<UserVisibleError>() {
                Some(UserVisibleError::Timeout) => {
                    reply.push_str(&format!(
                        "**{}**: timeout UserVisibleError::Timeout\n",
                        name
                    ));
                }
                None => {
                    error!("server_one: {:?}", e);
                    reply.push_str(&format!("**{}**: error\n", name));
                }
            },
        }
    }

    reply.push_str(&host_info);

    Ok(reply)
}

use chrono::{DateTime, Utc};
use serde::de;
use serde::de::Visitor;
use serde::Deserializer;
use serde::Serializer;
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Clone, Debug, PartialEq)]
pub enum GameState2 {
    InProgress,
    WaitingPostMatch,
    LeavingMap,
    EnteringMap,
    WaitingToStart,
    Unknown,
}

impl<'de> Deserialize<'de> for GameState2 {
    fn deserialize<D>(deserializer: D) -> Result<GameState2, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(GameState2Visitor)
    }
}

struct GameState2Visitor;

impl<'de> Visitor<'de> for GameState2Visitor {
    type Value = GameState2;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a string representing a game state")
    }

    fn visit_str<E>(self, value: &str) -> Result<GameState2, E>
    where
        E: de::Error,
    {
        match value {
            "InProgress" => Ok(GameState2::InProgress),
            "WaitingPostMatch" => Ok(GameState2::WaitingPostMatch),
            "LeavingMap" => Ok(GameState2::LeavingMap),
            "EnteringMap" => Ok(GameState2::EnteringMap),
            "WaitingToStart" => Ok(GameState2::WaitingToStart),
            _ => Ok(GameState2::Unknown), // Or return Err(E::custom("unknown game state"))
        }
    }
}

impl serde::Serialize for GameState2 {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let state = match *self {
            GameState2::InProgress => "InProgress",
            GameState2::WaitingPostMatch => "WaitingPostMatch",
            GameState2::LeavingMap => "LeavingMap",
            GameState2::EnteringMap => "EnteringMap",
            GameState2::WaitingToStart => "WaitingToStart",
            GameState2::Unknown => "Unknown",
        };
        serializer.serialize_str(state)
    }
}

#[tracing::instrument[skip(http_client, kv_snapshots)]]
pub async fn server(
    http_client: &reqwest_middleware::ClientWithMiddleware,
    kv_snapshots: &jetstream::kv::Store,
    is_admin_channel: bool,
) -> Result<String, Error> {
    trace!("server");

    let server = crate::get_server(&http_client, "tpg1".to_string()).await?;

    // destructure server
    let GetServerWithPlayersServer {
        // game_mode,
        map_name: current_map,
        // region,
        player_count: nb_players,
        // match_timeout,
        // game_version: version,
        // next_layer: next_map,
        // team_one,
        // team_two,
        // playtime_secs: uptime,
        // public_queue: nb_players_in_queue,
        // reserved_queue,
        // players,
        ..
    } = server;

    // get SquadServer server, error otherwise
    let server2 = match server.on {
        crate::queries::get_server_with_players::GetServerWithPlayersServerOn::SquadServer(
            server,
        ) => server,
        _ => bail!("error"),
    };

    // destructure server2
    let GetServerWithPlayersServerOnSquadServer {
        game_mode,
        public_queue: nb_players_in_queue,
        // nb_players_in_queue,
        // uptime,
        game_version: version,
        next_layer: next_map,
        region,
        // next_map,
        players,
        ..
    } = server2;

    //     let server2 = server.on;

    //     match server.on {
    //     crate::queries::get_server_with_players::GetServerWithPlayersServerOn::Squad44Server => bail!("error"),
    //     crate::queries::get_server_with_players::GetServerWithPlayersServerOn::SquadServer(server2) => {
    //         let GetServerWithPlayersServer2 {
    //             game_state: state,
    //             ..
    //         } = server2;
    //     }
    // }}

    if is_admin_channel {
        use crate::Admin;

        /*
         ... on OfflinePlayer {
               disconnectedSince
             }
        */

        // dbg!(&server);
        let mut admins: Vec<Admin> =
            players
            .into_iter()
            .filter_map(|v| {
                let offline_since = match v.on {
                    crate::queries::get_server_with_players::GetServerWithPlayersServerOnSquadServerPlayersOn::OfflinePlayer(p) => {
                        Some(p.disconnected_since)
                    }
                    crate::queries::get_server_with_players::GetServerWithPlayersServerOnSquadServerPlayersOn::OnlinePlayer => None,
                };

                if v.admin_group == Some("Admin".to_string()) {
                    Some(Admin {
                        name: PlayerName {
                            name: v.player_name.name,
                            clan_tag: v.player_name.clan_tag,
                        },
                        group: v.admin_group.unwrap(),
                        offline_since,
                    })
                } else {
                    None
                }
            })
            .collect();
        admins.sort_unstable_by_key(|v| v.name.name.clone());

        // dbg!(&admins);
        // todo!();

        use crate::TmplServerAdmins;
        let q = TmplServerAdmins {
            server_name: "tpg1".to_string(),
            game_type: crate::GameType::Squad,
            nb_players,
            admins,
            nb_players_in_queue,
            // prev_map: "n/a".to_string(),
            current_map,
            game_state: "n/a".to_string(),
            version,
            uptime: 0,
            next_map: next_map.unwrap_or_else(|| "n/a".to_string()),
            region,
            game_mode,
        };

        use askama::Template;
        let q2 = q.render()?;

        // Ok(format!("{:?}", snapshot))
        return Ok(q2);
    }

    use crate::TmplServerNoAdmins;
    // let q: TmplServerNoAdmins = snapshot.into();
    let q = TmplServerNoAdmins {
        server_name: "tpg1".to_string(),
        game_type: crate::GameType::Squad,
        nb_players,
        nb_players_in_queue,
        // prev_map: "n/a".to_string(),
        current_map,
        game_state: "n/a".to_string(),
        version,
        uptime: 0,
        next_map: next_map.unwrap_or_else(|| "n/a".to_string()),
        region,
        game_mode,
    };

    use askama::Template;
    let q2 = q.render()?;

    // Ok(format!("{:?}", snapshot))
    Ok(q2)
}

#[tracing::instrument[skip(http_client, kv_snapshots)]]
pub async fn server2(
    http_client: &reqwest_middleware::ClientWithMiddleware,
    kv_snapshots: &jetstream::kv::Store,
    is_admin_channel: bool,
) -> Result<String, Error> {
    trace!("server2");

    let server = crate::get_server(&http_client, "tpg2".to_string()).await?;

    // destructure server
    let GetServerWithPlayersServer {
        // game_mode,
        map_name: current_map,
        // region,
        player_count: nb_players,
        // match_timeout,
        // game_version: version,
        // next_layer: next_map,
        // team_one,
        // team_two,
        // playtime_secs: uptime,
        // public_queue: nb_players_in_queue,
        // reserved_queue,
        // players,
        ..
    } = server;

    // get SquadServer server, error otherwise
    let server2 = match server.on {
        crate::queries::get_server_with_players::GetServerWithPlayersServerOn::SquadServer(
            server,
        ) => server,
        _ => bail!("error"),
    };

    // destructure server2
    let GetServerWithPlayersServerOnSquadServer {
        game_mode,
        public_queue: nb_players_in_queue,
        // nb_players_in_queue,
        // uptime,
        game_version: version,
        next_layer: next_map,
        region,
        // next_map,
        players,
        ..
    } = server2;

    //     let server2 = server.on;

    //     match server.on {
    //     crate::queries::get_server_with_players::GetServerWithPlayersServerOn::Squad44Server => bail!("error"),
    //     crate::queries::get_server_with_players::GetServerWithPlayersServerOn::SquadServer(server2) => {
    //         let GetServerWithPlayersServer2 {
    //             game_state: state,
    //             ..
    //         } = server2;
    //     }
    // }}

    if is_admin_channel {
        use crate::Admin;

        /*
         ... on OfflinePlayer {
               disconnectedSince
             }
        */

        // dbg!(&server);
        let mut admins: Vec<Admin> =
            players
            .into_iter()
            .filter_map(|v| {
                let offline_since = match v.on {
                    crate::queries::get_server_with_players::GetServerWithPlayersServerOnSquadServerPlayersOn::OfflinePlayer(p) => {
                        Some(p.disconnected_since)
                    }
                    crate::queries::get_server_with_players::GetServerWithPlayersServerOnSquadServerPlayersOn::OnlinePlayer => None,
                };

                if v.admin_group == Some("Admin".to_string()) {
                    Some(Admin {
                        name: PlayerName {
                            name: v.player_name.name,
                            clan_tag: v.player_name.clan_tag,
                        },
                        group: v.admin_group.unwrap(),
                        offline_since,
                    })
                } else {
                    None
                }
            })
            .collect();
        admins.sort_unstable_by_key(|v| v.name.name.clone());

        // dbg!(&admins);
        // todo!();

        use crate::TmplServerAdmins;
        let q = TmplServerAdmins {
            server_name: "tpg2".to_string(),
            game_type: crate::GameType::Squad,
            nb_players,
            admins,
            nb_players_in_queue,
            // prev_map: "n/a".to_string(),
            current_map,
            game_state: "n/a".to_string(),
            version,
            uptime: 0,
            next_map: next_map.unwrap_or_else(|| "n/a".to_string()),
            region,
            game_mode,
        };

        use askama::Template;
        let q2 = q.render()?;

        // Ok(format!("{:?}", snapshot))
        return Ok(q2);
    }

    use crate::TmplServerNoAdmins;
    // let q: TmplServerNoAdmins = snapshot.into();
    let q = TmplServerNoAdmins {
        server_name: "tpg2".to_string(),
        game_type: crate::GameType::Squad,
        nb_players,
        nb_players_in_queue,
        // prev_map: "n/a".to_string(),
        current_map,
        game_state: "n/a".to_string(),
        version,
        uptime: 0,
        next_map: next_map.unwrap_or_else(|| "n/a".to_string()),
        region,
        game_mode,
    };

    use askama::Template;
    let q2 = q.render()?;

    // Ok(format!("{:?}", snapshot))
    Ok(q2)
}

#[tracing::instrument[skip(http_client, kv_snapshots)]]
pub async fn server_sq44(
    http_client: &reqwest_middleware::ClientWithMiddleware,
    kv_snapshots: &jetstream::kv::Store,
    is_admin_channel: bool,
) -> Result<String, Error> {
    trace!("server_sq44");

    let server = crate::get_server(&http_client, "sq44".to_string()).await?;

    // destructure server
    let GetServerWithPlayersServer {
        // game_mode,
        map_name: current_map,
        // region,
        player_count: nb_players,
        // match_timeout,
        // game_version: version,
        // next_layer: next_map,
        // team_one,
        // team_two,
        // playtime_secs: uptime,
        // public_queue: nb_players_in_queue,
        // reserved_queue,
        // players,
        ..
    } = server;

    let server2 = match server.on {
        crate::queries::get_server_with_players::GetServerWithPlayersServerOn::Squad44Server(
            server,
        ) => server,
        _ => bail!("error"),
    };

    // destructure server2
    let GetServerWithPlayersServerOnSquad44Server { players, .. } = server2;

    if is_admin_channel {
        use crate::Admin;

        /*
         ... on OfflinePlayer {
               disconnectedSince
             }
        */

        // dbg!(&server);
        let mut admins: Vec<Admin> =
            players
            .into_iter()
            .filter_map(|v| {
                let offline_since = match v.on {
                    crate::queries::get_server_with_players::GetServerWithPlayersServerOnSquad44ServerPlayersOn::Offline44Player(p) => {
                        Some(p.disconnected_since)
                    }
                    crate::queries::get_server_with_players::GetServerWithPlayersServerOnSquad44ServerPlayersOn::Online44Player => None,
                };

                if v.admin_group == Some("Admin".to_string()) {
                    Some(Admin {
                        name: PlayerName {
                            name: v.player_name,
                            clan_tag: None,
                        },
                        group: v.admin_group.unwrap(),
                        offline_since,
                    })
                } else {
                    None
                }
            })
            .collect();
        admins.sort_unstable_by_key(|v| v.name.name.clone());

        // dbg!(&admins);
        // todo!();

        use crate::TmplServerAdmins;
        let q = TmplServerAdmins {
            server_name: "sq44".to_string(),
            game_type: crate::GameType::Squad44,
            nb_players,
            admins,
            nb_players_in_queue: 0,
            // prev_map: "n/a".to_string(),
            current_map,
            game_state: "n/a".to_string(),
            version: "n/a".to_string(),
            uptime: 0,
            next_map: "n/a".to_string(),
            region: "n/a".to_string(),
            game_mode: "n/a".to_string(),
        };

        use askama::Template;
        let q2 = q.render()?;

        // Ok(format!("{:?}", snapshot))
        return Ok(q2);
    }

    use crate::TmplServerNoAdmins;
    // let q: TmplServerNoAdmins = snapshot.into();
    let q = TmplServerNoAdmins {
        server_name: "sq44".to_string(),
        game_type: crate::GameType::Squad44,
        nb_players,
        nb_players_in_queue: 0,
        // prev_map: "n/a".to_string(),
        current_map,
        game_state: "n/a".to_string(),
        version: "n/a".to_string(),
        uptime: 0,
        next_map: "n/a".to_string(),
        region: "n/a".to_string(),
        game_mode: "n/a".to_string(),
    };

    use askama::Template;
    let q2 = q.render()?;

    // Ok(format!("{:?}", snapshot))
    Ok(q2)
}
