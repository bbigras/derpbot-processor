use anyhow::Error;
use reqwest::Url;
use secrecy::{ExposeSecret, SecretString};
use serde::Deserialize;
use serde_json::json;
use tracing::trace;

use crate::HttpClient;

#[derive(Debug, Deserialize)]
pub struct DiscordReply {
    pub id: String,
    pub channel_id: String,
}

#[tracing::instrument(skip(http_client, token))]
pub async fn send_message(
    http_client: &HttpClient,
    channel_id: &str,
    message_id: &str,
    reply_msg: &str,
    token: &SecretString,
) -> Result<DiscordReply, Error> {
    trace!("send_message");

    // let url = format!(
    //     "https://discord.com/api/v10/channels/{}/messages",
    //     channel_id
    // );

    let url: Url = "https://discord.com/api/v10/channels/"
        .parse::<Url>()
        .unwrap()
        .join(&format!("{}/", channel_id))
        .unwrap()
        .join("messages")
        .unwrap();

    // let nonce = sf.next_id().unwrap();

    // TODO: nonce
    let response: DiscordReply = http_client
        .post(url)
        .header("Authorization", format!("Bot {}", token.expose_secret()))
        .header("Content-Type", "application/json")
        .body(
            json!({
                // "nonce": nonce.to_string(),
                "content": reply_msg,
                "message_reference": {
                    "message_id": message_id
                }
            })
            .to_string(),
        )
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;

    // TODO return message id
    // println!("response: {}", response.text().await?);

    /*
    response: "{\"id\":\"1170425957832724560\",\"type\":19,\"content\":\"tpg1: 41 players\\ntpg2: 17 players\",\"channel_id\":\"427624644988633088\",\"author\":{\"id\":\"272232004114841600\",\"username\":\"DerpBot\",\"avatar\":\"4b8997496a314c552ef45fbfe5e91ee7\",\"discriminator\":\"7379\",\"public_flags\":524288,\"premium_type\":0,\"flags\":524288,\"bot\":true,\"banner\":null,\"accent_color\":null,\"global_name\":null,\"avatar_decoration_data\":null,\"banner_color\":null},\"attachments\":[],\"embeds\":[],\"mentions\":[{\"id\":\"138093729717157888\",\"username\":\"brunoqc\",\"avatar\":\"28e846d598eca27d486957ab96f7b138\",\"discriminator\":\"0\",\"public_flags\":0,\"premium_type\":0,\"flags\":0,\"banner\":null,\"accent_color\":null,\"global_name\":\"brunoqc\",\"avatar_decoration_data\":null,\"banner_color\":null}],\"mention_roles\":[],\"pinned\":false,\"mention_everyone\":false,\"tts\":false,\"timestamp\":\"2023-11-04T18:14:53.810000+00:00\",\"edited_timestamp\":null,\"flags\":0,\"components\":[],\"message_reference\":{\"channel_id\":\"427624644988633088\",\"message_id\":\"1170425954322108437\",\"guild_id\":\"417484481964933130\"},\"referenced_message\":{\"id\":\"1170425954322108437\",\"type\":0,\"content\":\"~server\",\"channel_id\":\"427624644988633088\",\"author\":{\"id\":\"138093729717157888\",\"username\":\"brunoqc\",\"avatar\":\"28e846d598eca27d486957ab96f7b138\",\"discriminator\":\"0\",\"public_flags\":0,\"premium_type\":0,\"flags\":0,\"banner\":null,\"accent_color\":null,\"global_name\":\"brunoqc\",\"avatar_decoration_data\":null,\"banner_color\":null},\"attachments\":[],\"embeds\":[],\"mentions\":[],\"mention_roles\":[],\"pinned\":false,\"mention_everyone\":false,\"tts\":false,\"timestamp\":\"2023-11-04T18:14:52.973000+00:00\",\"edited_timestamp\":null,\"flags\":0,\"components\":[]}}\n"
    */

    Ok(response)
}

#[tracing::instrument(err, skip(http_client, token))]
pub async fn delete_message(
    http_client: &HttpClient,
    channel_id: &str,
    message_id: &str,
    token: &SecretString,
) -> Result<(), Error> {
    trace!("delete_message");

    // let url = format!(
    //     "https://discord.com/api/v10/channels/{}/messages/{}",
    //     channel_id, message_id
    // );

    let url: Url = "https://discord.com/api/v10/channels/"
        .parse::<Url>()
        .unwrap()
        .join(&format!("{}/", channel_id))
        .unwrap()
        .join("messages/")
        .unwrap()
        .join(&message_id.to_string())
        .unwrap();

    // let nonce = sf.next_id().unwrap();

    // TODO: nonce
    // let response: DiscordReply = http_client
    let _response = http_client
        .delete(url)
        .header("Authorization", format!("Bot {}", token.expose_secret()))
        .header("Content-Type", "application/json")
        .send()
        .await?
        .error_for_status()?;

    Ok(())
}

#[test]
fn test_url() {
    let channel_id = 123;
    let message_id = 456;

    let url = format!(
        "https://discord.com/api/v10/channels/{}/messages/{}",
        channel_id, message_id
    );

    assert_eq!(url, "https://discord.com/api/v10/channels/123/messages/456");

    use reqwest::Url;

    let url2: Url = "https://discord.com/api/v10/channels/"
        .parse::<Url>()
        .unwrap()
        .join(&format!("{}/", channel_id))
        .unwrap()
        .join("messages/")
        .unwrap()
        .join(&message_id.to_string())
        .unwrap();

    assert_eq!(
        url2.to_string(),
        "https://discord.com/api/v10/channels/123/messages/456"
    );
}
