use askama_escape::Escaper;

use std::{fmt, str};

pub struct Discord;

impl Escaper for Discord {
    fn write_escaped<W>(&self, mut fmt: W, string: &str) -> fmt::Result
    where
        W: fmt::Write,
    {
        let str2 = discord_escape_msg(string);
        fmt.write_str(&str2)
    }
}

#[tracing::instrument]
pub fn discord_escape_msg(msg: &str) -> String {
    let mut r = String::new();

    let bad = vec!['*', '_', '@', '`', '<', '>', ':', '/', '!'];

    for c in msg.chars() {
        if !c.is_ascii() || bad.contains(&c) {
            r.push('\\');
        }
        r.push(c);
    }

    r
}

#[test]
fn test_discord_escape_msg() {
    assert_eq!(
        discord_escape_msg("test *test2* test3"),
        "test \\*test2\\* test3".to_string()
    );

    assert_eq!(
        discord_escape_msg("123**test**456"),
        "123\\*\\*test\\*\\*456".to_string()
    );

    assert_eq!(discord_escape_msg("👍"), "\\👍".to_string());
}
