#![feature(extract_if)]

mod antispam;
mod commands;
mod custom_scalars;
mod discord;
mod discord_escape;
mod queries;
mod steam;

use anyhow::{anyhow, bail, Context, Error};
use askama::Template;
use async_nats::jetstream;
use async_nats::jetstream::consumer::DeliverPolicy;
use async_nats::jetstream::AckKind;
use async_nats::jetstream::{context::Context as JsContext, Message};
use chrono::{DateTime, Utc};
use clap::{Parser, Subcommand};
use dotenvy::dotenv;
use graphql_client::GraphQLQuery;
use serde_avro_fast::{from_single_object_slice, to_single_object, Schema};
use tokio::time::error::Elapsed;
use tokio::time::timeout;
use tracing::{debug, error, info, trace, warn};
// use futures_util::stream::stream::StreamExt;
use futures_util::{StreamExt, TryStreamExt};
use rand::prelude::*;
use reqwest_middleware::{ClientBuilder, ClientWithMiddleware};
use reqwest_retry::{policies::ExponentialBackoff, RetryTransientMiddleware};
use reqwest_tracing::TracingMiddleware;
use secrecy::{ExposeSecret, SecretString};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use crate::antispam::{DiscordMsgType, DiscordServerPost};
use crate::discord_escape::discord_escape_msg;

use std::env;
use std::fs::File;
use std::time::Duration;

const MAX_CONCURRENT: usize = 5;
const INDICATOR_AFTER_SECS: u64 = 2;
const COMMAND_TIMEOUT_SECS: u64 = 10;
const HTTP_TIMEOUT_MS: u64 = 1500;

type SteamID = u64;
type HttpClient = ClientWithMiddleware;
type AppID = i32;

const SQUAD_APP_ID: AppID = 393380;
// const PS_APP_ID: AppID = 736220;

mod filters {
    use super::*;

    #[tracing::instrument]
    pub fn duration_str2(timestamp: &DateTime<Utc>) -> askama::Result<String> {
        Ok(duration_str(&timestamp.signed_duration_since(Utc::now())))
    }

    #[tracing::instrument]
    pub fn to_timestamp(timestamp: &DateTime<Utc>) -> askama::Result<i64> {
        Ok(timestamp.timestamp())
    }
}

fn duration_str(dur: &chrono::Duration) -> String {
    let s0: f64 = dur.num_seconds().abs() as f64;

    let d: f64 = (s0 / (3600.0 * 24.0)).round();
    let h: f64 = ((s0 % (3600.0 * 24.0)) / 3600.0).round();
    let m: f64 = ((s0 % 3600.0) / 60.0).round();
    let s: f64 = s0 % 60.0;

    if d != 0.0 {
        return format!("{d}d");
    }
    if h != 0.0 {
        return format!("{h}h");
    }
    if m != 0.0 {
        return format!("{m}m");
    }

    format!("{s}s")
}

#[test]
fn test_duration_str() {
    let a = chrono::Duration::seconds(35 + 3600);
    assert_eq!(duration_str(&a), "1h");

    let a = chrono::Duration::seconds(6300);
    assert_eq!(duration_str(&a), "2h");
}

enum GameType {
    Squad,
    Squad44,
}

#[derive(Template)]
#[template(path = "server3admins.discord")]
struct TmplServerAdmins {
    server_name: String,
    game_type: GameType,
    nb_players: i64,
    nb_players_in_queue: i64,
    // prev_map: String,
    current_map: String,
    region: String,
    game_mode: String,
    next_map: String,
    game_state: String,
    version: String,
    uptime: i64,
    admins: Vec<Admin>,
}

#[derive(Template)]
#[template(path = "server3noadmins.discord")]
struct TmplServerNoAdmins {
    server_name: String,
    game_type: GameType,
    nb_players: i64,
    nb_players_in_queue: i64,
    // prev_map: String,
    current_map: String,
    region: String,
    game_mode: String,
    next_map: String,
    game_state: String,
    version: String,
    uptime: i64,
}

#[derive(Debug)]
struct PlayerName {
    pub name: String,
    pub clan_tag: Option<String>,
}

#[derive(Debug)]
struct Admin {
    name: PlayerName,
    group: String,
    offline_since: Option<chrono::DateTime<Utc>>,
}

#[derive(Error, Debug)]
pub enum UserVisibleError {
    #[error("Timeout")]
    Timeout,
    // #[error("Invalid input: {0}")]
    // InvalidInput(String),

    // ... other errors
}

#[tracing::instrument(skip(http_client))]
async fn handle_ping(
    http_client: &ClientWithMiddleware,
    message: &Message,
    token: &SecretString,
) -> Result<(), Error> {
    trace!("handle_ping");

    let pid = std::process::id();
    let binding = hostname::get().unwrap();
    let hostname = binding.to_str().unwrap();

    use async_nats::jetstream::AckKind;
    use std::time::Duration;
    // message.ack().await.unwrap();
    // message.ack_with(AckKind::Nak(None)).await?;

    // if rng.gen_range(0..10) > 7 {
    //     error!("simulate error -> nack");
    //     message.ack_with(AckKind::Nak(Some(Duration::new(2, 0)))).await.unwrap();
    //     continue;
    // }

    let channel_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-channel-id")
        .unwrap()
        .to_string();
    let message_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-message-id")
        .unwrap()
        .to_string();

    use serde_json::json;

    let reply_msg = format!("pong! (hostname={}, pid={})", hostname, pid);

    let url = format!(
        "https://discord.com/api/v10/channels/{}/messages",
        channel_id
    );

    // let nonce = sf.next_id().unwrap();

    // TODO: nonce
    let _response = http_client
        .post(&url)
        .header("Authorization", format!("Bot {}", token.expose_secret()))
        .header("Content-Type", "application/json")
        .body(
            json!({
                // "nonce": nonce.to_string(),
                "content": reply_msg,
                "message_reference": {
                    "message_id": message_id
                }
            })
            .to_string(),
        )
        .send()
        .await?
        .error_for_status()?;

    // println!("response: {:#?}", response.text().await?);

    Ok(())
}

#[tracing::instrument(skip(http_client, nats_client, kv_servers_squad, message, token))]
async fn handle_server(
    http_client: &ClientWithMiddleware,
    nats_client: &async_nats::Client,
    kv_servers_squad: &jetstream::kv::Store,
    message: &Message,
    token: &SecretString,
) -> Result<(), Error> {
    info!("handle_server");

    // let pid = std::process::id();
    // let binding = hostname::get().unwrap();
    // let hostname = binding.to_str().unwrap();

    // use async_nats::jetstream::AckKind;
    // use std::time::Duration;
    // message.ack().await.unwrap();
    // message.ack_with(AckKind::Nak(None)).await?;

    // if rng.gen_range(0..10) > 7 {
    //     error!("simulate error -> nack");
    //     message.ack_with(AckKind::Nak(Some(Duration::new(2, 0)))).await.unwrap();
    //     continue;
    // }

    let message_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-message-id")
        .unwrap()
        .to_string();

    let channel_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-channel-id")
        .unwrap()
        .to_string();

    let is_admin_channel = [
        "469225312769671168",
        "417503665293754378",
        "746135651484696669",
        "469226619228127232",
        "677140958046978049",
        "417498936660590602",
        "420048418346500097",
    ]
    .contains(&channel_id.as_str());

    let command = "server";

    if let Err(e) = antispam::post_nats_sp(
        nats_client,
        DiscordServerPost {
            command: command.to_string(),
            msg_id: message_id.clone(),
            channel_id: channel_id.clone(),
            msg_type: DiscordMsgType::Request,
        },
    )
    .await
    {
        error!("post_nats_sp: {:?}", e);
    }

    use tokio::time::timeout;

    match timeout(
        Duration::from_secs(COMMAND_TIMEOUT_SECS),
        commands::server::server(&http_client, kv_servers_squad, is_admin_channel),
    )
    .await
    {
        Ok(r) => match r {
            Ok(reply_msg) => {
                let discord_reply =
                    discord::send_message(http_client, &channel_id, &message_id, &reply_msg, token)
                        .await?;

                if let Err(e) = antispam::post_nats_sp(
                    nats_client,
                    DiscordServerPost {
                        command: command.to_string(),
                        msg_id: discord_reply.id,
                        channel_id: discord_reply.channel_id,
                        msg_type: DiscordMsgType::Reply,
                    },
                )
                .await
                {
                    error!("post_nats_sp: {:?}", e);
                }
            }
            Err(e) => {
                error!("error: {:?}", e);
                let discord_reply =
                    discord::send_message(http_client, &channel_id, &message_id, "err", token)
                        .await?;

                if let Err(e) = antispam::post_nats_sp(
                    nats_client,
                    DiscordServerPost {
                        command: command.to_string(),
                        msg_id: discord_reply.id,
                        channel_id: discord_reply.channel_id,
                        msg_type: DiscordMsgType::Reply,
                    },
                )
                .await
                {
                    error!("post_nats_sp: {:?}", e);
                }
            }
        },
        Err(_e) => {
            error!("timeout");
            let discord_reply =
                discord::send_message(http_client, &channel_id, &message_id, "timeout", token)
                    .await?;

            if let Err(e) = antispam::post_nats_sp(
                nats_client,
                DiscordServerPost {
                    command: command.to_string(),
                    msg_id: discord_reply.id,
                    channel_id: discord_reply.channel_id,
                    msg_type: DiscordMsgType::Reply,
                },
            )
            .await
            {
                error!("post_nats_sp: {:?}", e);
            }
        }
    }

    Ok(())
}

#[tracing::instrument(skip(http_client, nats_client, kv_servers_squad, message, token))]
async fn handle_server2(
    http_client: &ClientWithMiddleware,
    nats_client: &async_nats::Client,
    kv_servers_squad: &jetstream::kv::Store,
    message: &Message,
    token: &SecretString,
) -> Result<(), Error> {
    info!("handle_server2");

    // let pid = std::process::id();
    // let binding = hostname::get().unwrap();
    // let hostname = binding.to_str().unwrap();

    // use async_nats::jetstream::AckKind;
    // use std::time::Duration;
    // message.ack().await.unwrap();
    // message.ack_with(AckKind::Nak(None)).await?;

    // if rng.gen_range(0..10) > 7 {
    //     error!("simulate error -> nack");
    //     message.ack_with(AckKind::Nak(Some(Duration::new(2, 0)))).await.unwrap();
    //     continue;
    // }

    let message_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-message-id")
        .unwrap()
        .to_string();

    let channel_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-channel-id")
        .unwrap()
        .to_string();

    let is_admin_channel = [
        "469225312769671168",
        "417503665293754378",
        "746135651484696669",
        "469226619228127232",
        "677140958046978049",
        "417498936660590602",
        "420048418346500097",
    ]
    .contains(&channel_id.as_str());

    let command = "server2";

    if let Err(e) = antispam::post_nats_sp(
        nats_client,
        DiscordServerPost {
            command: command.to_string(),
            msg_id: message_id.clone(),
            channel_id: channel_id.clone(),
            msg_type: DiscordMsgType::Request,
        },
    )
    .await
    {
        error!("post_nats_sp: {:?}", e);
    }

    use tokio::time::timeout;

    match timeout(
        Duration::from_secs(COMMAND_TIMEOUT_SECS),
        commands::server::server2(&http_client, kv_servers_squad, is_admin_channel),
    )
    .await
    {
        Ok(r) => match r {
            Ok(reply_msg) => {
                let discord_reply =
                    discord::send_message(http_client, &channel_id, &message_id, &reply_msg, token)
                        .await?;

                if let Err(e) = antispam::post_nats_sp(
                    nats_client,
                    DiscordServerPost {
                        command: command.to_string(),
                        msg_id: discord_reply.id,
                        channel_id: discord_reply.channel_id,
                        msg_type: DiscordMsgType::Reply,
                    },
                )
                .await
                {
                    error!("post_nats_sp: {:?}", e);
                }
            }
            Err(e) => {
                error!("error: {:?}", e);
                let discord_reply =
                    discord::send_message(http_client, &channel_id, &message_id, "err", token)
                        .await?;

                if let Err(e) = antispam::post_nats_sp(
                    nats_client,
                    DiscordServerPost {
                        command: command.to_string(),
                        msg_id: discord_reply.id,
                        channel_id: discord_reply.channel_id,
                        msg_type: DiscordMsgType::Reply,
                    },
                )
                .await
                {
                    error!("post_nats_sp: {:?}", e);
                }
            }
        },
        Err(_e) => {
            error!("timeout");
            let discord_reply =
                discord::send_message(http_client, &channel_id, &message_id, "timeout", token)
                    .await?;

            if let Err(e) = antispam::post_nats_sp(
                nats_client,
                DiscordServerPost {
                    command: command.to_string(),
                    msg_id: discord_reply.id,
                    channel_id: discord_reply.channel_id,
                    msg_type: DiscordMsgType::Reply,
                },
            )
            .await
            {
                error!("post_nats_sp: {:?}", e);
            }
        }
    }

    Ok(())
}

#[tracing::instrument(skip(http_client, nats_client, kv_servers_squad, message, token))]
async fn handle_sq44(
    http_client: &ClientWithMiddleware,
    nats_client: &async_nats::Client,
    kv_servers_squad: &jetstream::kv::Store,
    message: &Message,
    token: &SecretString,
) -> Result<(), Error> {
    info!("handle_sq44");

    // let pid = std::process::id();
    // let binding = hostname::get().unwrap();
    // let hostname = binding.to_str().unwrap();

    // use async_nats::jetstream::AckKind;
    // use std::time::Duration;
    // message.ack().await.unwrap();
    // message.ack_with(AckKind::Nak(None)).await?;

    // if rng.gen_range(0..10) > 7 {
    //     error!("simulate error -> nack");
    //     message.ack_with(AckKind::Nak(Some(Duration::new(2, 0)))).await.unwrap();
    //     continue;
    // }

    let message_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-message-id")
        .unwrap()
        .to_string();

    let channel_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-channel-id")
        .unwrap()
        .to_string();

    let is_admin_channel = [
        "469225312769671168",
        "417503665293754378",
        "746135651484696669",
        "469226619228127232",
        "677140958046978049",
        "417498936660590602",
        "420048418346500097",
    ]
    .contains(&channel_id.as_str());

    let command = "sq44";

    // anti-spam
    if let Err(e) = antispam::post_nats_sp(
        nats_client,
        DiscordServerPost {
            command: command.to_string(),
            msg_id: message_id.clone(),
            channel_id: channel_id.clone(),
            msg_type: DiscordMsgType::Request,
        },
    )
    .await
    {
        error!("post_nats_sp: {:?}", e);
    }

    use tokio::time::timeout;

    match timeout(
        Duration::from_secs(COMMAND_TIMEOUT_SECS),
        commands::server::server_sq44(&http_client, kv_servers_squad, is_admin_channel),
    )
    .await
    {
        Ok(r) => match r {
            Ok(reply_msg) => {
                let discord_reply =
                    discord::send_message(http_client, &channel_id, &message_id, &reply_msg, token)
                        .await?;

                if let Err(e) = antispam::post_nats_sp(
                    nats_client,
                    DiscordServerPost {
                        command: command.to_string(),
                        msg_id: discord_reply.id,
                        channel_id: discord_reply.channel_id,
                        msg_type: DiscordMsgType::Reply,
                    },
                )
                .await
                {
                    error!("post_nats_sp: {:?}", e);
                }
            }
            Err(e) => {
                error!("error: {:?}", e);
                let discord_reply =
                    discord::send_message(http_client, &channel_id, &message_id, "err", token)
                        .await?;

                if let Err(e) = antispam::post_nats_sp(
                    nats_client,
                    DiscordServerPost {
                        command: command.to_string(),
                        msg_id: discord_reply.id,
                        channel_id: discord_reply.channel_id,
                        msg_type: DiscordMsgType::Reply,
                    },
                )
                .await
                {
                    error!("post_nats_sp: {:?}", e);
                }
            }
        },
        Err(_e) => {
            error!("timeout");
            let discord_reply =
                discord::send_message(http_client, &channel_id, &message_id, "timeout", token)
                    .await?;

            if let Err(e) = antispam::post_nats_sp(
                nats_client,
                DiscordServerPost {
                    command: command.to_string(),
                    msg_id: discord_reply.id,
                    channel_id: discord_reply.channel_id,
                    msg_type: DiscordMsgType::Reply,
                },
            )
            .await
            {
                error!("post_nats_sp: {:?}", e);
            }
        }
    }

    Ok(())
}

#[tracing::instrument(skip(http_client))]
async fn handle_t15(
    http_client: &ClientWithMiddleware,
    message: &Message,
    token: &SecretString,
) -> Result<(), Error> {
    trace!("handle_t15");

    let pid = std::process::id();
    let binding = hostname::get().unwrap();
    let hostname = binding.to_str().unwrap();

    use async_nats::jetstream::AckKind;
    use std::time::Duration;
    // message.ack().await.unwrap();
    // message.ack_with(AckKind::Nak(None)).await?;

    // if rng.gen_range(0..10) > 7 {
    //     error!("simulate error -> nack");
    //     message.ack_with(AckKind::Nak(Some(Duration::new(2, 0)))).await.unwrap();
    //     continue;
    // }

    let channel_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-channel-id")
        .unwrap()
        .to_string();
    let message_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-message-id")
        .unwrap()
        .to_string();

    let timestamp: u64 = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-timestamp")
        .unwrap()
        .to_string()
        .parse()
        .unwrap();

    let channel_id_clone = channel_id.clone();
    let http_client_clone = http_client.clone();
    let token_clone = token.clone();

    let handle = tokio::spawn(async move {
        use std::time::SystemTime;

        // sleep_until(Instant::now() + Duration::from_millis(100)).await;
        let system_time = SystemTime::UNIX_EPOCH + Duration::new(timestamp, 0);
        // dbg!(&system_time);

        let elapsed = system_time.elapsed().unwrap();
        // dbg!(&elapsed);

        let diff = Duration::new(INDICATOR_AFTER_SECS, 0).checked_sub(elapsed);
        // dbg!(&diff);

        // let dur = SystemTime::now().checked_sub(system_time);

        if let Some(diff2) = diff {
            tokio::time::sleep(diff2).await;
        }

        let url = format!(
            "https://discord.com/api/v10/channels/{}/typing",
            channel_id_clone
        );

        let _response = http_client_clone
            .post(&url)
            .header(
                "Authorization",
                format!("Bot {}", token_clone.expose_secret()),
            )
            // .header("Content-Type", "application/json")
            .header("Content-length", "0")
            .send()
            .await
            .unwrap()
            .error_for_status()
            .unwrap();
    });

    use serde_json::json;

    let reply_msg = format!("15 secs elapsed! (hostname={}, pid={})", hostname, pid);

    let url = format!(
        "https://discord.com/api/v10/channels/{}/messages",
        channel_id
    );

    use tokio::time::sleep;
    sleep(tokio::time::Duration::from_millis(15_000)).await;

    handle.abort();

    // let nonce = sf.next_id().unwrap();

    // TODO: nonce
    let _response = http_client
        .post(&url)
        .header("Authorization", format!("Bot {}", token.expose_secret()))
        .header("Content-Type", "application/json")
        .body(
            json!({
                // "nonce": nonce.to_string(),
                "content": reply_msg,
                "message_reference": {
                    "message_id": message_id
                }
            })
            .to_string(),
        )
        .send()
        .await?
        .error_for_status()?;

    // println!("response: {:#?}", response.text().await?);

    Ok(())
}

#[tracing::instrument(skip(http_client))]
async fn handle_t5(
    http_client: &ClientWithMiddleware,
    message: &Message,
    token: &SecretString,
) -> Result<(), Error> {
    trace!("handle_t5");

    let pid = std::process::id();
    let binding = hostname::get().unwrap();
    let hostname = binding.to_str().unwrap();

    use async_nats::jetstream::AckKind;
    use std::time::Duration;
    // message.ack().await.unwrap();
    // message.ack_with(AckKind::Nak(None)).await?;

    // if rng.gen_range(0..10) > 7 {
    //     error!("simulate error -> nack");
    //     message.ack_with(AckKind::Nak(Some(Duration::new(2, 0)))).await.unwrap();
    //     continue;
    // }

    let channel_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-channel-id")
        .unwrap()
        .to_string();
    let message_id = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-message-id")
        .unwrap()
        .to_string();

    let timestamp: u64 = message
        .headers
        .as_ref()
        .unwrap()
        .get("X-timestamp")
        .unwrap()
        .to_string()
        .parse()
        .unwrap();

    let channel_id_clone = channel_id.clone();
    let http_client_clone = http_client.clone();
    let token_clone = token.clone();

    let handle = tokio::spawn(async move {
        use std::time::SystemTime;

        // sleep_until(Instant::now() + Duration::from_millis(100)).await;
        let system_time = SystemTime::UNIX_EPOCH + Duration::new(timestamp, 0);
        // dbg!(&system_time);

        let elapsed = system_time.elapsed().unwrap();
        // dbg!(&elapsed);

        let diff = Duration::new(INDICATOR_AFTER_SECS, 0).checked_sub(elapsed);
        // dbg!(&diff);

        // let dur = SystemTime::now().checked_sub(system_time);

        if let Some(diff2) = diff {
            tokio::time::sleep(diff2).await;
        }

        let url = format!(
            "https://discord.com/api/v10/channels/{}/typing",
            channel_id_clone
        );

        let _response = http_client_clone
            .post(&url)
            .header(
                "Authorization",
                format!("Bot {}", token_clone.expose_secret()),
            )
            // .header("Content-Type", "application/json")
            .header("Content-length", "0")
            .send()
            .await
            .unwrap()
            .error_for_status()
            .unwrap();
    });

    use serde_json::json;

    let reply_msg = format!("5 secs elapsed! (hostname={}, pid={})", hostname, pid);

    let url = format!(
        "https://discord.com/api/v10/channels/{}/messages",
        channel_id
    );

    use tokio::time::sleep;
    sleep(tokio::time::Duration::from_millis(5_000)).await;

    handle.abort();

    // let nonce = sf.next_id().unwrap();

    // TODO: nonce
    let _response = http_client
        .post(&url)
        .header("Authorization", format!("Bot {}", token.expose_secret()))
        .header("Content-Type", "application/json")
        .body(
            json!({
                // "nonce": nonce.to_string(),
                "content": reply_msg,
                "message_reference": {
                    "message_id": message_id
                }
            })
            .to_string(),
        )
        .send()
        .await?
        .error_for_status()?;

    // println!("response: {:#?}", response.text().await?);

    Ok(())
}

#[tracing::instrument(
    err,
    skip(http_client, nats_client, kv_servers_squad, message, token),
    fields(channel_id, message_id)
)]
async fn process_message(
    http_client: &ClientWithMiddleware,
    nats_client: &async_nats::Client,
    kv_servers_squad: &jetstream::kv::Store,
    message: &Message,
    token: &SecretString,
) -> Result<(), Error> {
    info!("process_message");
    // trace!("process_message");

    // TODO: log msg type

    // let channel_id = message
    //     .headers
    //     .as_ref()
    //     .unwrap()
    //     .get("X-channel-id")
    //     .unwrap()
    //     .to_string();
    // let message_id = message
    //     .headers
    //     .as_ref()
    //     .unwrap()
    //     .get("X-message-id")
    //     .unwrap()
    //     .to_string();

    // tracing::Span::current().record("channel_id", &channel_id);
    // tracing::Span::current().record("message_id", &message_id);

    // TODO: test progress ack, nack...

    // println!("message receiver: {:?}", message);
    // println!("got 1 msg. subject={}", message.subject);

    match message.subject.as_str() {
        "derpbot.commands" => match &*message.payload {
            b"ping" => {
                println!("ping!");
                handle_ping(http_client, message, token).await?;
            }
            b"t5" => {
                println!("t5!");
                handle_t5(http_client, message, token).await?;
            }
            b"t15" => {
                println!("t15!");
                handle_t15(http_client, message, token).await?;
            }
            b"server" => {
                handle_server(http_client, nats_client, kv_servers_squad, message, token).await?;
            }
            b"server2" => {
                handle_server2(http_client, nats_client, kv_servers_squad, message, token).await?;
            }
            b"sq44" => {
                handle_sq44(http_client, nats_client, kv_servers_squad, message, token).await?;
            }
            _ => {
                warn!("unknown command: {:?}", message.payload);
            }
        },
        _ => {
            warn!("unknown subject: {}", message.subject);
        }
    };

    Ok(())
}

#[tracing::instrument(skip_all)]
async fn f_server(
    nats_client: async_nats::Client,
    jetstream: JsContext,
    http_client: ClientWithMiddleware,
    token: SecretString,
) -> Result<(), Error> {
    let kv_servers_squad = jetstream.get_key_value("servers-squad").await?;

    // TODO: group?

    let stream = jetstream.get_stream("derpbot_commands").await?;

    let consumer = stream
        .get_or_create_consumer(
            "consumer",
            async_nats::jetstream::consumer::pull::Config {
                durable_name: Some("consumer".to_string()),
                ..Default::default()
            },
        )
        .await?;

    // subject: "derpbot.commands.server"

    let messages = consumer.messages().await?;

    info!("waiting");
    messages
        .for_each_concurrent(MAX_CONCURRENT, |msg| {
            let token = token.clone();
            let http_client = http_client.clone();
            let nats_client = nats_client.clone();
            let kv_servers_squad = kv_servers_squad.clone();

            async move {
                match msg {
                    Ok(message) => {
                        match process_message(
                            &http_client,
                            &nats_client,
                            &kv_servers_squad,
                            &message,
                            &token,
                        )
                        .await
                        {
                            Ok(()) => {
                                message.ack().await.unwrap();
                            }
                            Err(e) => {
                                error!("process_message: {:?}", e);
                                // TODO: error with retry=false?
                                message
                                    .ack_with(AckKind::Nak(Some(Duration::new(2, 0))))
                                    .await
                                    .unwrap();
                            }
                        }
                    }
                    Err(e) => {
                        error!("message: {:?}", e);
                    }
                }
            }
        })
        .await;

    Ok(())
}

#[tracing::instrument(skip(http_client))]
async fn get_server(
    http_client: &ClientWithMiddleware,
    id: String,
) -> Result<queries::get_server_with_players::GetServerWithPlayersServer, Error> {
    info!("get_server");

    let variables = queries::get_server_with_players::Variables { id };

    let request_body = queries::GetServerWithPlayers::build_query(variables);

    let url = env::var("GRAPHQL_URL").context("GRAPHQL_URL not defined")?;

    let response_body: graphql_client::Response<queries::get_server_with_players::ResponseData> =
        http_client
            .post(url)
            .json(&request_body)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?;

    if let Some(errors) = response_body.errors {
        bail!("err: {:?}", errors);
    }

    let r = response_body
        .data
        // .context("no data")
        .unwrap()
        .server;

    if let Some(e) = response_body.errors {
        bail!("err: {:?}", e);
    }

    Ok(r)
}

#[derive(Parser, Debug)]
struct Args {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Debug, Subcommand)]
enum Commands {
    TestCmdServer {},
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    match main2().await {
        Ok(()) => {
            info!("main2() ok2");
        }
        Err(e) => {
            error!("main2(): {:?}", e);
            bail!("main2(): {:?}", e);
        }
    }

    Ok(())
}

async fn main2() -> Result<(), Error> {
    let args = Args::parse();

    dotenv().ok();

    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env().unwrap_or_else(|_| {
                // axum logs rejections from built-in extractors with the `axum::rejection`
                // target, at `TRACE` level. `axum::rejection=trace` enables showing those events
		"async_nats::connector=debug,derpbot_processor=debug,tower_http=debug,axum::rejection=trace,tower_http::trace::on_request=info,tower_http::trace::on_response=info,info".into()
            }),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    info!("launch");

    // let reply_msg = commands::server::server().await?;
    // dbg!(&reply_msg);
    // todo!();

    // use snowflake_me::Snowflake;
    // let sf = Snowflake::new().unwrap();

    let token: SecretString = env::var("DISCORD_TOKEN")
        .context("DISCORD_TOKEN not defined")?
        .into();

    let nats_creds = env::var("NATS_CREDS").ok();
    let nats_servers = env::var("NATS_SERVERS").context("NATS_SERVERS not defined")?;

    let rng = thread_rng();

    let reqwest_client = reqwest::Client::builder()
        .timeout(Duration::from_millis(HTTP_TIMEOUT_MS))
        .build()
        .unwrap();

    let retry_policy = ExponentialBackoff::builder().build_with_max_retries(3);
    let http_client = ClientBuilder::new(reqwest_client)
        // .with_init(Extension(OtelName("my-client".into())))
        .with(TracingMiddleware::default())
        .with(RetryTransientMiddleware::new_with_policy(retry_policy))
        .build();

    info!("nats_servers={:?}", nats_servers);

    let nats_servers: Vec<async_nats::ServerAddr> = nats_servers
        .split(',')
        .into_iter()
        .map(|v| {
            let r = v.parse::<async_nats::ServerAddr>()?;
            Ok::<_, Error>(r)
        })
        .collect::<Result<Vec<async_nats::ServerAddr>, Error>>()?;

    let mut nats_options = async_nats::ConnectOptions::new().retry_on_initial_connect();

    if let Some(creds) = nats_creds {
        info!("with creds: {}", creds);
        nats_options = nats_options.credentials_file(creds).await?;
    }

    let nats_client = timeout(Duration::from_secs(5), nats_options.connect(nats_servers)).await??;

    let jetstream = async_nats::jetstream::new(nats_client.clone());

    match args.command {
        Some(Commands::TestCmdServer {}) => {
            let id = "tpg1".to_string();
            let s = get_server(&http_client, id).await?;
            dbg!(&s);
            return Ok(());
        }
        None => {
            // info!("no command");
        }
    }
    // ---
    // let kv_snapshots = jetstream.get_key_value("snapshots").await?;
    // let s = commands::server::server(&http_client, &kv_snapshots, true).await?;
    // // dbg!(&s);
    // println!("reply: {}", s);

    // todo!();

    let f1 = f_server(
        nats_client.clone(),
        jetstream.clone(),
        http_client.clone(),
        token.clone(),
    );
    let f2 = antispam::f_server2(/*nats_client.clone(),*/ jetstream, http_client, token);

    // f2.await.unwrap();

    tokio::select! {
        _ = f1 => (),
        _ = f2 => (),
    }

    use tokio::time::sleep;
    sleep(tokio::time::Duration::from_millis(15_000)).await;

    Ok(())
}
