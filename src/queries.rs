#![allow(clippy::all, warnings)]
pub struct GetServer;
pub mod get_server {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "GetServer";
    pub const QUERY : & str = "query GetServer($id: String!) {\n    server(id: $id) {\n        id\n        __typename\n        maxPlayers\n        mapName\n        playerCount\n        ... on SquadServer {\n            gameMode\n            region\n            matchTimeout\n            gameVersion\n            nextLayer\n            teamOne\n            teamTwo\n            playtimeSecs\n            publicQueue\n            reservedQueue\n        }\n    }\n}\n\nquery GetServerWithPlayers($id: String!) {\n    server(id: $id) {\n        id\n        __typename\n        maxPlayers\n        mapName\n        playerCount\n        ... on SquadServer {\n            gameMode\n            region\n            matchTimeout\n            gameVersion\n            nextLayer\n            teamOne\n            teamTwo\n            playtimeSecs\n            publicQueue\n            reservedQueue\n            players {\n                __typename\n                id\n                steamId\n                playerName {\n                    clanTag\n                    name\n                }\n                adminGroup\n                ... on OfflinePlayer {\n                    disconnectedSince\n                }\n            }\n        }\n... on Squad44Server {\n      name\n      players {\n        id\n        __typename\n        steamId\n        playerName\n        adminGroup\n        ... on Offline44Player {\n          disconnectedSince\n        }\n      }\n    }\n    }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    #[derive(Serialize, Debug)]
    pub struct Variables {
        pub id: String,
    }
    impl Variables {}
    #[derive(Deserialize, Clone, Debug)]
    pub struct ResponseData {
        pub server: GetServerServer,
    }
    #[derive(Deserialize, Clone, Debug)]
    pub struct GetServerServer {
        pub id: ID,
        #[serde(rename = "maxPlayers")]
        pub max_players: Int,
        #[serde(rename = "mapName")]
        pub map_name: String,
        #[serde(rename = "playerCount")]
        pub player_count: Int,
        #[serde(flatten)]
        pub on: GetServerServerOn,
    }
    #[derive(Deserialize, Clone, Debug)]
    #[serde(tag = "__typename")]
    pub enum GetServerServerOn {
        Squad44Server,
        SquadServer(GetServerServerOnSquadServer),
    }
    #[derive(Deserialize, Clone, Debug)]
    pub struct GetServerServerOnSquadServer {
        #[serde(rename = "gameMode")]
        pub game_mode: String,
        pub region: String,
        #[serde(rename = "matchTimeout")]
        pub match_timeout: Int,
        #[serde(rename = "gameVersion")]
        pub game_version: String,
        #[serde(rename = "nextLayer")]
        pub next_layer: Option<String>,
        #[serde(rename = "teamOne")]
        pub team_one: String,
        #[serde(rename = "teamTwo")]
        pub team_two: String,
        #[serde(rename = "playtimeSecs")]
        pub playtime_secs: Int,
        #[serde(rename = "publicQueue")]
        pub public_queue: Int,
        #[serde(rename = "reservedQueue")]
        pub reserved_queue: Int,
    }
}
impl graphql_client::GraphQLQuery for GetServer {
    type Variables = get_server::Variables;
    type ResponseData = get_server::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: get_server::QUERY,
            operation_name: get_server::OPERATION_NAME,
        }
    }
}
pub struct GetServerWithPlayers;
pub mod get_server_with_players {
    #![allow(dead_code)]
    use std::result::Result;
    pub const OPERATION_NAME: &str = "GetServerWithPlayers";
    pub const QUERY : & str = "query GetServer($id: String!) {\n    server(id: $id) {\n        id\n        __typename\n        maxPlayers\n        mapName\n        playerCount\n        ... on SquadServer {\n            gameMode\n            region\n            matchTimeout\n            gameVersion\n            nextLayer\n            teamOne\n            teamTwo\n            playtimeSecs\n            publicQueue\n            reservedQueue\n        }\n    }\n}\n\nquery GetServerWithPlayers($id: String!) {\n    server(id: $id) {\n        id\n        __typename\n        maxPlayers\n        mapName\n        playerCount\n        ... on SquadServer {\n            gameMode\n            region\n            matchTimeout\n            gameVersion\n            nextLayer\n            teamOne\n            teamTwo\n            playtimeSecs\n            publicQueue\n            reservedQueue\n            players {\n                __typename\n                id\n                steamId\n                playerName {\n                    clanTag\n                    name\n                }\n                adminGroup\n                ... on OfflinePlayer {\n                    disconnectedSince\n                }\n            }\n        }\n... on Squad44Server {\n      name\n      players {\n        id\n        __typename\n        steamId\n        playerName\n        adminGroup\n        ... on Offline44Player {\n          disconnectedSince\n        }\n      }\n    }\n    }\n}\n" ;
    use super::*;
    use serde::{Deserialize, Serialize};
    #[allow(dead_code)]
    type Boolean = bool;
    #[allow(dead_code)]
    type Float = f64;
    #[allow(dead_code)]
    type Int = i64;
    #[allow(dead_code)]
    type ID = String;
    type DateTime = crate::custom_scalars::DateTime;
    type SteamID = crate::custom_scalars::SteamID;
    #[derive(Serialize, Debug)]
    pub struct Variables {
        pub id: String,
    }
    impl Variables {}
    #[derive(Deserialize, Clone, Debug)]
    pub struct ResponseData {
        pub server: GetServerWithPlayersServer,
    }
    #[derive(Deserialize, Clone, Debug)]
    pub struct GetServerWithPlayersServer {
        pub id: ID,
        #[serde(rename = "maxPlayers")]
        pub max_players: Int,
        #[serde(rename = "mapName")]
        pub map_name: String,
        #[serde(rename = "playerCount")]
        pub player_count: Int,
        #[serde(flatten)]
        pub on: GetServerWithPlayersServerOn,
    }
    #[derive(Deserialize, Clone, Debug)]
    #[serde(tag = "__typename")]
    pub enum GetServerWithPlayersServerOn {
        Squad44Server(GetServerWithPlayersServerOnSquad44Server),
        SquadServer(GetServerWithPlayersServerOnSquadServer),
    }
    #[derive(Deserialize, Clone, Debug)]
    pub struct GetServerWithPlayersServerOnSquad44Server {
        pub name: String,
        pub players: Vec<GetServerWithPlayersServerOnSquad44ServerPlayers>,
    }
    #[derive(Deserialize, Clone, Debug)]
    pub struct GetServerWithPlayersServerOnSquad44ServerPlayers {
        pub id: Int,
        #[serde(rename = "steamId")]
        pub steam_id: SteamID,
        #[serde(rename = "playerName")]
        pub player_name: String,
        #[serde(rename = "adminGroup")]
        pub admin_group: Option<String>,
        #[serde(flatten)]
        pub on: GetServerWithPlayersServerOnSquad44ServerPlayersOn,
    }
    #[derive(Deserialize, Clone, Debug)]
    #[serde(tag = "__typename")]
    pub enum GetServerWithPlayersServerOnSquad44ServerPlayersOn {
        Offline44Player(GetServerWithPlayersServerOnSquad44ServerPlayersOnOffline44Player),
        Online44Player,
    }
    #[derive(Deserialize, Clone, Debug)]
    pub struct GetServerWithPlayersServerOnSquad44ServerPlayersOnOffline44Player {
        #[serde(rename = "disconnectedSince")]
        pub disconnected_since: DateTime,
    }
    #[derive(Deserialize, Clone, Debug)]
    pub struct GetServerWithPlayersServerOnSquadServer {
        #[serde(rename = "gameMode")]
        pub game_mode: String,
        pub region: String,
        #[serde(rename = "matchTimeout")]
        pub match_timeout: Int,
        #[serde(rename = "gameVersion")]
        pub game_version: String,
        #[serde(rename = "nextLayer")]
        pub next_layer: Option<String>,
        #[serde(rename = "teamOne")]
        pub team_one: String,
        #[serde(rename = "teamTwo")]
        pub team_two: String,
        #[serde(rename = "playtimeSecs")]
        pub playtime_secs: Int,
        #[serde(rename = "publicQueue")]
        pub public_queue: Int,
        #[serde(rename = "reservedQueue")]
        pub reserved_queue: Int,
        pub players: Vec<GetServerWithPlayersServerOnSquadServerPlayers>,
    }
    #[derive(Deserialize, Clone, Debug)]
    pub struct GetServerWithPlayersServerOnSquadServerPlayers {
        pub id: Int,
        #[serde(rename = "steamId")]
        pub steam_id: SteamID,
        #[serde(rename = "playerName")]
        pub player_name: GetServerWithPlayersServerOnSquadServerPlayersPlayerName,
        #[serde(rename = "adminGroup")]
        pub admin_group: Option<String>,
        #[serde(flatten)]
        pub on: GetServerWithPlayersServerOnSquadServerPlayersOn,
    }
    #[derive(Deserialize, Clone, Debug)]
    #[serde(tag = "__typename")]
    pub enum GetServerWithPlayersServerOnSquadServerPlayersOn {
        OfflinePlayer(GetServerWithPlayersServerOnSquadServerPlayersOnOfflinePlayer),
        OnlinePlayer,
    }
    #[derive(Deserialize, Clone, Debug)]
    pub struct GetServerWithPlayersServerOnSquadServerPlayersOnOfflinePlayer {
        #[serde(rename = "disconnectedSince")]
        pub disconnected_since: DateTime,
    }
    #[derive(Deserialize, Clone, Debug)]
    pub struct GetServerWithPlayersServerOnSquadServerPlayersPlayerName {
        #[serde(rename = "clanTag")]
        pub clan_tag: Option<String>,
        pub name: String,
    }
}
impl graphql_client::GraphQLQuery for GetServerWithPlayers {
    type Variables = get_server_with_players::Variables;
    type ResponseData = get_server_with_players::ResponseData;
    fn build_query(variables: Self::Variables) -> ::graphql_client::QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: get_server_with_players::QUERY,
            operation_name: get_server_with_players::OPERATION_NAME,
        }
    }
}
