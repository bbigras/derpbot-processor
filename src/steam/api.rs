use anyhow::{Context, Error};
use reqwest::Url;
use secrecy::{ExposeSecret, SecretString};
use serde::Deserialize;

use crate::{AppID, HttpClient, SteamID};

#[derive(Deserialize, Debug)]
struct ResponseT1 {
    steamid: Option<String>,
    message: Option<String>,
    success: i32,
}

#[derive(Deserialize, Debug)]
struct DataT1 {
    response: ResponseT1,
}

#[tracing::instrument(skip(http_client))]
async fn get_steam_id(
    http_client: &HttpClient,
    c_url: &str,
    steam_key: SecretString,
) -> Result<u64, Error> {
    let mut url = Url::parse("https://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/")?;
    url.query_pairs_mut()
        .append_pair("key", &steam_key.expose_secret())
        .append_pair("vanityurl", c_url);

    let data: DataT1 = http_client
        .get(url)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;

    let steam_id = data.response.steamid.context("no steam id")?.parse()?;

    Ok(steam_id)
}

#[derive(Deserialize, Debug)]
struct Player {
    steamid: String,
    personaname: String,
    profileurl: String,
    avatar: String,
    avatarmedium: String,
    avatarfull: String,
    avatarhash: String,
}

#[derive(Deserialize, Debug)]
struct Response {
    players: Vec<Player>,
}

#[derive(Deserialize, Debug)]
struct PlayerSummariesRep {
    response: Response,
}

#[tracing::instrument(skip(http_client))]
async fn get_player(
    http_client: &HttpClient,
    steam_key: SecretString,
    steam_id: SteamID,
) -> Result<Player, Error> {
    let mut url = Url::parse("https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/")?;
    url.query_pairs_mut()
        .append_pair("key", &steam_key.expose_secret())
        .append_pair("steamids", &steam_id.to_string());

    let players: PlayerSummariesRep = http_client
        .get(url)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;

    let player = players.response.players.into_iter().next().unwrap();

    Ok(player)
}

#[derive(Deserialize, Debug)]
pub struct PlayersBansPlayer {
    #[serde(rename = "SteamId")]
    steam_id: String,
    #[serde(rename = "CommunityBanned")]
    pub community_banned: bool,
    #[serde(rename = "VACBanned")]
    pub vac_banned: bool,
    #[serde(rename = "NumberOfVACBans")]
    pub number_of_vac_bans: i32,
    #[serde(rename = "DaysSinceLastBan")]
    pub days_since_last_ban: i32,
    #[serde(rename = "NumberOfGameBans")]
    pub number_of_game_bans: i32,
    #[serde(rename = "EconomyBan")]
    pub economy_ban: String,
}

#[derive(Deserialize, Debug)]
struct PlayersBansData {
    players: Vec<PlayersBansPlayer>,
}

#[tracing::instrument(skip(http_client))]
pub async fn get_player_bans(
    http_client: &HttpClient,
    steam_key: SecretString,
    steam_id: SteamID,
) -> Result<PlayersBansPlayer, Error> {
    let mut url = Url::parse("https://api.steampowered.com/ISteamUser/GetPlayerBans/v1/")?;
    url.query_pairs_mut()
        .append_pair("key", &steam_key.expose_secret())
        .append_pair("steamids", &steam_id.to_string());

    let players: PlayersBansData = http_client
        .get(url)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;

    let player = players.players.into_iter().next().unwrap();

    Ok(player)
}

#[derive(Deserialize, Debug)]
struct OwnedGameData {
    response: OwnedGameResponse,
}

#[derive(Deserialize, Debug)]
struct OwnedGameResponse {
    games: Option<Vec<OwnedGame>>,
}

#[derive(Deserialize, Debug)]
pub struct OwnedGame {
    pub appid: i32,
    pub playtime_2weeks: Option<i64>,
    pub playtime_forever: i64,
}

#[tracing::instrument(skip(http_client))]
pub async fn get_play_time(
    http_client: &HttpClient,
    steam_key: SecretString,
    steam_id: SteamID,
    app_ids: &[AppID],
) -> Result<Vec<OwnedGame>, Error> {
    let mut url = Url::parse("https://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/")?;
    url.query_pairs_mut()
        .append_pair("key", &steam_key.expose_secret())
        .append_pair("steamid", &steam_id.to_string());

    let data: OwnedGameData = http_client
        .get(url)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;

    let r: Vec<OwnedGame> = data
        .response
        .games
        .unwrap_or_default()
        .into_iter()
        .filter(|game| app_ids.contains(&game.appid))
        .collect();

    Ok(r)
}

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[tokio::test]
//     async fn test_get_player() -> Result<(), Error> {
//         let http_client = reqwest::Client::new();

//         let steam_key = std::env::var("STEAM_KEY").unwrap().into();

//         let player = get_player(&http_client, steam_key, 76561197984704834).await?;

//         dbg!(&player);

//         todo!();
//     }

//     #[tokio::test]
//     async fn test_get_player_bans() -> Result<(), Error> {
//         let http_client = reqwest::Client::new();

//         let steam_key = std::env::var("STEAM_KEY").unwrap().into();

//         let player = get_player_bans(&http_client, steam_key, 76561197984704834).await?;

//         dbg!(&player);

//         todo!();
//     }
// }
