use async_trait::async_trait;

use anyhow::{anyhow, Context, Error};
use tokio::net::UdpSocket;
use tracing::{debug, info, trace, warn};

use nom::combinator::{all_consuming, rest};
use nom::{
    bytes::complete::{tag, take_until},
    number::complete::{le_f32, le_i16, le_i32, le_i64, le_u8},
    IResult,
};

use core::fmt::Debug;
use std::boxed::Box;
use std::io::{self, Read, Write};
use std::net::SocketAddr;

use byteorder::LittleEndian;
use byteorder::WriteBytesExt;

// use nom::{
// nom_err
//, parse_byte, parse_float, parse_header, parse_long, parse_long_long, parse_short,
// };

// use crate::{

//     parse_string, write_byte, write_long, write_string, A2sType, A2sTypeRep,
// };

#[inline]
fn nom_err<T>(e: nom::Err<T>) -> Error
where
    T: std::fmt::Debug,
{
    anyhow!("nom error: {:?}", e)
}

#[repr(u8)]
#[derive(PartialEq)]
enum A2sType {
    CHALLENGE = 0x57,
    INFO = 0x54,
    PING = 0x69,
    PLAYER = 0x55,
    RULES = 0x56,
}

#[repr(u8)]
#[derive(Debug, PartialEq)]
enum A2sTypeRep {
    CHALLENGE = 0x41,
    INFO = 0x49,
    PLAYER = 0x44,
    RULES = 0x45,
}

impl From<u8> for A2sType {
    #[tracing::instrument]
    fn from(v: u8) -> Self {
        // TODO: match
        if v == (A2sType::INFO as u8) {
            A2sType::INFO
        } else if v == (A2sType::PLAYER as u8) {
            A2sType::PLAYER
        } else if v == (A2sType::CHALLENGE as u8) {
            A2sType::CHALLENGE
        } else if v == (A2sType::RULES as u8) {
            A2sType::RULES
        } else {
            warn!("unknown A2sType: {}", v);
            todo!("{}", v)
        }
    }
}

#[inline]
fn write_long(resp: &mut Vec<u8>, n: i32) -> io::Result<()> {
    let r = resp.write_i32::<LittleEndian>(n)?;
    Ok(r)
}

#[inline]
fn write_long_long(resp: &mut Vec<u8>, n: i64) -> io::Result<()> {
    let r = resp.write_i64::<LittleEndian>(n)?;
    Ok(r)
}

#[inline]
fn write_float(resp: &mut Vec<u8>, n: f32) -> io::Result<()> {
    let r = resp.write_f32::<LittleEndian>(n)?;
    Ok(r)
}

#[inline]
fn write_byte(resp: &mut Vec<u8>, n: u8) -> io::Result<()> {
    let r = resp.write_u8(n)?;
    Ok(r)
}

// #[inline]
// fn write_int(resp: &mut Vec<u8>, n: u32) -> io::Result<()> {
//     let r = resp.write_u32::<LittleEndian>(n)?;
//     Ok(r)
// }

#[inline]
fn write_short(resp: &mut Vec<u8>, n: i16) -> io::Result<()> {
    let r = resp.write_i16::<LittleEndian>(n)?;
    Ok(r)
}

#[inline]
fn write_string(resp: &mut Vec<u8>, data: &str) -> io::Result<()> {
    for one in data.as_bytes() {
        resp.write_u8(*one)?;
    }
    let r = resp.write_u8(0x0)?;
    Ok(r)
}

#[tracing::instrument]
fn parse_header(input: &[u8]) -> IResult<&[u8], ()> {
    let (input, i) = parse_long(input)?;

    if i != -1 {
        return Err(nom::Err::Failure(nom::error::Error::new(
            input,
            nom::error::ErrorKind::Fail,
        )));
    }

    Ok((input, ()))
}

#[tracing::instrument]
fn parse_byte(input: &[u8]) -> IResult<&[u8], u8> {
    le_u8(input)
}

#[tracing::instrument]
fn parse_short(input: &[u8]) -> IResult<&[u8], i16> {
    le_i16(input)
}

#[tracing::instrument]
fn parse_long(input: &[u8]) -> IResult<&[u8], i32> {
    le_i32(input)
}

#[tracing::instrument]
fn parse_long_long(input: &[u8]) -> IResult<&[u8], i64> {
    le_i64(input)
}

#[tracing::instrument]
fn parse_float(input: &[u8]) -> IResult<&[u8], f32> {
    le_f32(input)
}

#[tracing::instrument]
fn parse_string(input: &[u8]) -> IResult<&[u8], String> {
    let (input, v) = take_until("\0")(input)?;
    let (input, _) = tag("\0")(input)?;
    let val = String::from_utf8_lossy(v).to_string();
    Ok((input, val))
}

#[tracing::instrument]
fn parse_string_bin(input: &[u8]) -> IResult<&[u8], &[u8]> {
    let (input, v) = take_until("\0")(input)?;
    let (input, _) = tag("\0")(input)?;
    Ok((input, v))
}

#[async_trait]
pub trait OneSocket: Sync + Debug {
    async fn connect(&self, addr: SocketAddr) -> io::Result<()>;
    async fn send(&self, buf: &[u8]) -> io::Result<usize>;
    async fn send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<usize>;
    async fn recv(&mut self, buf: &mut [u8]) -> io::Result<usize>;
    async fn recv_from(&mut self, buf: &mut [u8]) -> io::Result<(usize, SocketAddr)>;
}

#[derive(Debug)]
pub struct MySocket {
    pub socket: UdpSocket,
}

#[async_trait]
impl OneSocket for MySocket {
    async fn send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<usize> {
        self.socket.send_to(buf, target).await
    }

    async fn recv(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.socket.recv(buf).await
    }

    async fn recv_from(&mut self, buf: &mut [u8]) -> io::Result<(usize, SocketAddr)> {
        self.socket.recv_from(buf).await
    }

    async fn connect(&self, addr: SocketAddr) -> io::Result<()> {
        self.socket.connect(addr).await
    }

    async fn send(&self, buf: &[u8]) -> io::Result<usize> {
        self.socket.send(buf).await
    }
}

#[derive(Debug)]
struct MySocketRec {
    socket: UdpSocket,
    nb_packet: u8,
    label: String,
}

#[async_trait]
impl OneSocket for MySocketRec {
    async fn connect(&self, addr: SocketAddr) -> io::Result<()> {
        self.socket.connect(addr).await
    }

    async fn send(&self, buf: &[u8]) -> io::Result<usize> {
        self.socket.send(buf).await
    }

    async fn send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<usize> {
        self.socket.send_to(buf, target).await
    }

    async fn recv(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let got = self.socket.recv(buf).await?;
        // dbg!(&buf[0..got]);

        let mut file =
            std::fs::File::create(format!("tests/{}-{}.bin", self.label, self.nb_packet))?;
        file.write_all(&buf[0..got])?;
        self.nb_packet += 1;

        Ok(got)
    }

    async fn recv_from(&mut self, buf: &mut [u8]) -> io::Result<(usize, SocketAddr)> {
        let (l, addr) = self.socket.recv_from(buf).await?;
        // dbg!(&buf[0..l]);

        let mut file =
            std::fs::File::create(format!("tests/{}-{}.bin", self.label, self.nb_packet))?;
        file.write_all(&buf[0..l])?;
        self.nb_packet += 1;

        Ok((l, addr))
    }
}

#[derive(Debug)]
struct MySocketReplay {
    socket: UdpSocket,
    nb_packet: u8,
    label: String,
}

#[async_trait]
impl OneSocket for MySocketReplay {
    async fn connect(&self, addr: SocketAddr) -> io::Result<()> {
        self.socket.connect(addr).await
    }

    async fn send(&self, buf: &[u8]) -> io::Result<usize> {
        self.socket.send(buf).await
    }

    async fn send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<usize> {
        self.socket.send_to(buf, target).await
    }

    async fn recv(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let mut file = std::fs::File::open(format!("tests/{}-{}.bin", self.label, self.nb_packet))?;
        let q = file.read(buf)?;
        self.nb_packet += 1;
        Ok(q)
    }

    async fn recv_from(&mut self, buf: &mut [u8]) -> io::Result<(usize, SocketAddr)> {
        let mut file = std::fs::File::open(format!("tests/{}-{}.bin", self.label, self.nb_packet))?;
        let l = file.read(buf)?;
        self.nb_packet += 1;
        let addr = "100.90.17.102:1234".parse().unwrap();
        Ok((l, addr))
    }
}

#[derive(Debug, PartialEq)]
pub struct A2sInfo {
    pub protocol_version: u8,
    pub name: String,
    pub map: String,
    pub folder: String,
    pub game: String,
    pub id: i16,
    pub broken_nb_players: u8,
    pub nb_players: Option<u8>,
    pub slots: Option<i8>,
    pub max_players: u8,
    pub bots: u8,
    pub server_type: u8,
    pub env: u8,
    pub visibility: u8, // lic/private
    pub vac: u8,
    pub version: String,
    pub extra: u8,
    pub keywords: Option<String>,
    pub port: Option<i16>,
    pub steam_id: Option<i64>,
    pub spectator: Option<(i16, String)>, // TODO: remove
    pub game_id: Option<i64>,
}

#[tracing::instrument(skip(input))]
fn parse_a2s_rules(input: &[u8]) -> IResult<&[u8], Vec<(String, String)>> {
    let (input, _) = parse_header(input)?;
    let (input, header) = parse_byte(input)?;
    if header != (A2sTypeRep::RULES as u8) {
        println!("wrong header (rules): {:02x?}", header);
        return Err(nom::Err::Failure(nom::error::Error::new(
            input,
            nom::error::ErrorKind::Fail,
        )));
    }

    let (mut input, nb_rules) = parse_short(input)?;

    let mut result = vec![];

    for _n in 0..nb_rules {
        let (input2, r) = parse_a2s_rules_one(input)?;
        input = input2;
        result.push(r);
    }
    Ok((input, result))
}

fn parse_a2s_rules_one(input: &[u8]) -> IResult<&[u8], (String, String)> {
    // debug!("parse_a2s_rules_one: len={}", input.len());
    let (input, str1) = parse_string(input)?;
    let (input, str2) = parse_string(input)?;
    Ok((input, (str1, str2)))
}

// TODO rename INFO
#[tracing::instrument(skip(input))]
fn parse_a2s_server(input: &[u8]) -> IResult<&[u8], A2sInfo> {
    // println!("parse_a2s_server: {:02x?}", input);

    let (input, _) = parse_header(input)?;
    let (input, header) = parse_byte(input)?;
    if header != (A2sTypeRep::INFO as u8) {
        println!("wrong header (server): {:02x?}", header);
        return Err(nom::Err::Failure(nom::error::Error::new(
            input,
            nom::error::ErrorKind::Fail,
        )));
    }
    let (input, protocol_version) = parse_byte(input)?;
    let (input, name) = parse_string(input)?;
    let (input, map) = parse_string(input)?;
    let (input, folder) = parse_string(input)?;
    let (input, game) = parse_string(input)?;
    let (input, id) = parse_short(input)?;
    let (input, broken_nb_players) = parse_byte(input)?;
    let (input, max_players) = parse_byte(input)?;
    let (input, bots) = parse_byte(input)?;
    let (input, server_type) = parse_byte(input)?;
    let (input, env) = parse_byte(input)?;
    let (input, visibility) = parse_byte(input)?;
    let (input, vac) = parse_byte(input)?;
    let (input, version) = parse_string(input)?;
    let (mut input, extra) = parse_byte(input)?;

    let mut steam_id = None;
    let mut game_id = None;
    let spectator = None;
    let mut port = None;

    let mut nb_players = None;
    let mut slots = None;

    if extra & 0x80 != 0 {
        let (new_input, val) = parse_short(input)?;
        input = new_input;
        port = Some(val)
    }

    if extra & 0x10 != 0 {
        let (new_input, val) = parse_long_long(input)?;
        input = new_input;
        steam_id = Some(val)
    }

    // if extra & 0x40 != 0 {
    //     panic!("spectator");
    //     let (nouv_rest, val) = le_u16(rest17)?;
    //     let (nouv_rest2, val2) = take_until("\0")(nouv_rest)?;
    //     let (nouv_rest3, _) = tag("\0")(nouv_rest2)?;

    //     let val3 = String::from_utf8_lossy(val2);

    //     println!("spec: {} {}", val, val3);

    //     rest17 = nouv_rest3;
    //     // spectator = Some((val, val2))
    // }

    let mut keywords = None;

    if extra & 0x20 != 0 {
        let (input2, val) = parse_string(input)?;
        keywords = Some(val.to_string());
        input = input2;
        // dbg!(&val);

        for one in val.split(',') {
            if one.starts_with("NUMOPENPRIVCONN:") {
                slots = Some(
                    one.trim_start_matches("NUMOPENPRIVCONN:")
                        .parse()
                        .with_context(|| format!("can't parse NUMOPENPRIVCONN: {}", one))
                        .map_err(|_e| {
                            nom::Err::Failure(nom::error::Error::new(
                                input,
                                nom::error::ErrorKind::Fail,
                            ))
                        })?,
                );
            } else if one.starts_with("NUMOPENCONN:") {
                nb_players = Some(
                    one.trim_start_matches("NUMOPENCONN:")
                        .parse::<i8>()
                        .with_context(|| format!("can't parse NUMOPENCONN: {}", one))
                        .map_err(|_e| {
                            nom::Err::Failure(nom::error::Error::new(
                                input,
                                nom::error::ErrorKind::Fail,
                            ))
                        })?,
                );
            }
        }
    }

    let nb_players = nb_players.map(|p| {
        let q: u8 = (max_players as i8 - p as i8 - slots.unwrap_or_default()) as u8;
        q
    });

    if extra & 0x01 != 0 {
        let (new_input, val) = parse_long_long(input)?;
        input = new_input;
        game_id = Some(val)
    }

    Ok((
        input,
        A2sInfo {
            broken_nb_players,
            protocol_version,
            name,
            map,
            folder,
            game,
            id,
            nb_players,
            max_players,
            bots,
            server_type,
            env,
            visibility,
            vac,
            version,
            port,
            steam_id,
            spectator,
            game_id,
            slots,
            extra,
            keywords,
        },
    ))
}

#[derive(Debug)]
pub struct Fetcher {
    pub socket: Box<dyn OneSocket + Send>,
}

impl Fetcher {
    #[tracing::instrument(skip(self))]
    pub async fn get_server_info(
        &mut self,
        addr: SocketAddr,
        with_challenge: bool,
    ) -> Result<A2sInfo, Error> {
        debug!("get_server_info");

        let mut status = vec![];
        write_long(&mut status, -1)?;
        write_byte(&mut status, A2sType::INFO as u8)?;
        write_string(&mut status, "Source Engine Query")?;
        write_long(&mut status, -1)?;

        let _sent = self
            .socket
            .send_to(&status, addr)
            .await
            .context("socket send 1")?;

        let local_ip: std::net::IpAddr = [127, 0, 0, 1].into();
        if addr.ip() != local_ip && with_challenge {
            // ---------------
            // Challenge
            let mut buf = [0; 1400];
            let amt = self.socket.recv(&mut buf).await.context("socket recv 1")?;
            // dbg!(&amt);
            let (_rest, parsed) = all_consuming(parse_challenge)(&buf[..amt])
                .map_err(|e| anyhow!("parse_challenge: {:?}", e))?;

            let mut resp: Vec<u8> = vec![];
            write_long(&mut resp, -1)?;
            write_byte(&mut resp, A2sType::INFO as u8)?;
            write_string(&mut resp, "Source Engine Query")?;
            write_long(&mut resp, parsed.challenge)?;

            self.socket
                .send_to(&resp, addr)
                .await
                .context("socket send 2")?;
        }

        let mut buf = vec![0; 1400];
        let (n, _peer) = self
            .socket
            .recv_from(&mut buf)
            .await
            .context("socket recv 2")?;

        let buf2 = &buf[0..n];

        let (_rest, zzz) = all_consuming(parse_a2s_server)(buf2)
            .map_err(nom_err)
            .with_context(|| format!("buf2={:02x?}", buf2))?;

        Ok(zzz)
    }

    #[tracing::instrument(skip(self))]
    pub async fn get_number_players(&mut self, addr: SocketAddr) -> Result<u8, Error> {
        trace!("get_number_players");

        self.socket.connect(addr).await?;

        let mut status = vec![];
        write_long(&mut status, -1)?;
        write_byte(&mut status, A2sType::PLAYER as u8)?;
        write_long(&mut status, -1)?;

        self.socket.send(&status).await?;

        let mut buf = [0; 100];

        let amt = self.socket.recv(&mut buf).await?;

        let (_rest, parsed) = all_consuming(parse_challenge)(&buf[..amt])
            .map_err(|e| anyhow!("parse_challenge: {:?}", e))?;

        let mut resp: Vec<u8> = vec![];
        write_long(&mut resp, -1)?;
        write_byte(&mut resp, A2sType::PLAYER as u8)?;
        write_long(&mut resp, parsed.challenge)?;

        self.socket.send(&resp).await?;

        // max 2527
        let mut buf2 = [0; 9600];

        let amt = self.socket.recv(&mut buf2).await?;

        let (_rest, parsed) = all_consuming(parse_a2s_player)(&buf2[..amt])
            .map_err(|e| anyhow!("parse_a2s_player: {:?}", e))?;

        let len2 = parsed.players.iter().filter(|e| !e.name.is_empty()).count();

        Ok(len2 as u8)
    }

    #[tracing::instrument(skip(self))]
    pub async fn get_players(&mut self, addr: SocketAddr) -> Result<Vec<A2sPlayer2>, Error> {
        debug!("query2: get_players");

        self.socket.connect(addr).await?;

        let mut status = vec![];
        write_long(&mut status, -1)?;
        write_byte(&mut status, A2sType::PLAYER as u8)?;
        write_long(&mut status, -1)?;

        self.socket.send(&status).await?;

        let local_ip: std::net::IpAddr = [127, 0, 0, 1].into();
        if addr.ip() != local_ip {
            let mut buf = [0; 100];

            let amt = self.socket.recv(&mut buf).await?;

            let (_rest, parsed) = all_consuming(parse_challenge)(&buf[..amt])
                .map_err(|e| anyhow!("parse_challenge: {:?}", e))?;

            let mut resp: Vec<u8> = vec![];
            write_long(&mut resp, -1)?;
            write_byte(&mut resp, A2sType::PLAYER as u8)?;
            write_long(&mut resp, parsed.challenge)?;

            self.socket.send(&resp).await?;
        }

        // max 2527
        let mut buf2 = [0; 9600];

        let amt = self.socket.recv(&mut buf2).await?;

        let (_rest, parsed) = all_consuming(parse_a2s_player)(&buf2[..amt])
            .map_err(|e| anyhow!("parse_a2s_player: {:?}", e))?;

        Ok(parsed.players)
    }

    #[tracing::instrument(skip(self))]
    pub async fn get_rules(&mut self, addr: SocketAddr) -> Result<Vec<(String, String)>, Error> {
        debug!("get_rules");

        self.socket.connect(addr).await?;

        let mut status = vec![];

        write_long(&mut status, -1)?;
        write_byte(&mut status, A2sType::RULES as u8)?;
        write_long(&mut status, -1)?;

        self.socket.send(&status).await?;

        let local_ip: std::net::IpAddr = [127, 0, 0, 1].into();
        if addr.ip() != local_ip {
            let mut buf = [0; 100];

            let amt = self.socket.recv(&mut buf).await?;

            let (_rest, parsed) = all_consuming(parse_challenge)(&buf[..amt])
                .map_err(|e| anyhow!("parse_challenge: {:?}", e))?;

            let mut resp: Vec<u8> = vec![];
            write_long(&mut resp, -1)?;
            write_byte(&mut resp, A2sType::RULES as u8)?;
            write_long(&mut resp, parsed.challenge)?;

            self.socket.send(&resp).await?;
        }

        // max 2527
        let mut buf2 = [0; 9600];

        let amt = self.socket.recv(&mut buf2).await?;

        let (_rest, parsed) = all_consuming(parse_a2s_rules)(&buf2[..amt])
            .map_err(|e| anyhow!("parse_a2s_rules: {:?}", e))?;

        Ok(parsed)
    }

    #[tracing::instrument(skip(self))]
    pub async fn ping(&mut self, addr: SocketAddr) -> Result<Vec<A2sPlayer2>, Error> {
        debug!("ping");
        self.socket.connect(addr).await?;

        let mut status = vec![];

        write_long(&mut status, -1)?;
        write_byte(&mut status, A2sType::PING as u8)?;

        self.socket.send(&status).await?;

        // max 2527
        let mut buf2 = [0; 9600];

        let _amt = self.socket.recv(&mut buf2).await?;

        todo!();
    }
}

#[derive(Debug)]
struct Challenge {
    challenge: i32,
}

#[tracing::instrument(skip(input))]
fn parse_challenge(input: &[u8]) -> IResult<&[u8], Challenge> {
    let (input, _) = parse_header(input)?;
    let (input, header) = parse_byte(input)?;
    if header != (A2sTypeRep::CHALLENGE as u8) {
        println!("wrong header (challenge): {:02x?}", header);
        return Err(nom::Err::Failure(nom::error::Error::new(
            input,
            nom::error::ErrorKind::Fail,
        )));
    }

    let (input, challenge) = parse_long(input)?;
    Ok((input, Challenge { challenge }))
}

#[derive(Debug, PartialEq)]
pub struct A2sPlayer2 {
    pub index: u8,
    pub name: String,
    pub score: i32,
    pub duration: f32,
}

struct A2sPlayer {
    nb_players: u8,
    players: Vec<A2sPlayer2>,
}

#[tracing::instrument(skip(input))]
fn parse_a2s_player(input: &[u8]) -> IResult<&[u8], A2sPlayer> {
    let (input, _) = parse_header(input)?;
    let (input, val) = parse_byte(input)?;

    if val != (A2sTypeRep::PLAYER as u8) {
        return Err(nom::Err::Failure(nom::error::Error::new(
            input,
            nom::error::ErrorKind::Fail,
        )));
    }

    let (mut input, nb_players) = parse_byte(input)?;

    let mut players = vec![];

    for _n in 0..nb_players {
        let (input2, p) = parse_a2s_player2(input)?;
        players.push(p);
        input = input2;
    }

    Ok((
        input,
        A2sPlayer {
            nb_players,
            players,
        },
    ))
}

#[tracing::instrument(skip(input))]
fn parse_a2s_player2(input: &[u8]) -> IResult<&[u8], A2sPlayer2> {
    let (input, index) = parse_byte(input)?;
    let (input, name) = parse_string(input)?;
    let (input, score) = parse_long(input)?;
    let (input, duration) = parse_float(input)?;

    Ok((
        input,
        A2sPlayer2 {
            index,
            name,
            score,
            duration,
        },
    ))
}

#[cfg(test)]
mod tests {
    use super::*;

    /*
        #[tokio::test]
        async fn test_get_server_info() -> Result<(), Error> {
            let socket = UdpSocket::bind("0.0.0.0:0").await?;

            let socket = Box::new(MySocketReplay {
                socket,
                nb_packet: 0,
                label: "get_server_info".to_string(),
            });
            let mut f = Fetcher { socket };
            let ip: std::net::IpAddr = "100.90.17.102".parse()?;
            let port: u16 = 27165;
            let r = f.get_server_info((ip, port).into()).await?;
            dbg!(&r);

            let expect = A2sInfo {
                protocol_version: 17,
                name: "   The Playground 1 | New Players Welcome | tpg1".to_string(),
                map: "LashkarValley_TC_v2".to_string(),
                folder: "squad".to_string(),
                game: "Squad".to_string(),
                id: 0,
                nb_players: None,
                slots: Some(2),
                max_players: 100,
                bots: 0,
                server_type: 100,
                env: 119,
                visibility: 0,
                vac: 0,
                version: "dev".to_string(),
                port: Some(7787),
                steam_id: Some(541027336),
                spectator: None,
                game_id: Some(393380),
                extra: 255,
                keywords: None,
                broken_nb_players: 0,
            };

            assert_eq!(expect, r);
            Ok(())
        }
    */

    #[tokio::test]
    async fn test_get_number_players() -> Result<(), Error> {
        let socket = UdpSocket::bind("0.0.0.0:0").await?;

        let socket = Box::new(MySocketReplay {
            // let socket = Box::new(MySocketRec {
            socket,
            nb_packet: 0,
            label: "get_number_players".to_string(),
        });
        let mut f = Fetcher { socket };
        let ip: std::net::IpAddr = "92.118.18.108".parse()?;
        let port: u16 = 27165;
        let r = f.get_number_players((ip, port).into()).await?;

        // assert_eq!(r, 98);
        assert_eq!(r, 0);
        Ok(())
    }

    #[tokio::test]
    async fn test_get_number_players2() -> Result<(), Error> {
        let socket = UdpSocket::bind("0.0.0.0:0").await?;

        let socket = Box::new(MySocketReplay {
            // let socket = Box::new(MySocketRec {
            socket,
            nb_packet: 0,
            label: "get_number_players2".to_string(),
        });
        let mut f = Fetcher { socket };
        let ip: std::net::IpAddr = "92.118.18.108".parse()?;
        let port: u16 = 27165;
        let r = f.get_number_players((ip, port).into()).await?;

        // assert_eq!(r, 98);
        assert_eq!(r, 27);
        Ok(())
    }

    #[tokio::test]
    async fn test_get_players() -> Result<(), Error> {
        let socket = UdpSocket::bind("0.0.0.0:0").await?;

        let socket = Box::new(MySocketReplay {
            socket,
            nb_packet: 0,
            label: "get_players".to_string(),
        });
        let mut f = Fetcher { socket };
        let ip: std::net::IpAddr = "92.118.18.108".parse()?;
        let port: u16 = 27165;
        let r = f.get_players((ip, port).into()).await?;

        assert_eq!(r.len(), 0);

        Ok(())
    }

    #[tokio::test]
    async fn test_get_players2() -> Result<(), Error> {
        let socket = UdpSocket::bind("0.0.0.0:0").await?;

        let socket = Box::new(MySocketReplay {
            socket,
            nb_packet: 0,
            label: "get_players2".to_string(),
        });
        let mut f = Fetcher { socket };
        let ip: std::net::IpAddr = "92.118.18.108".parse()?;
        let port: u16 = 27165;
        let r = f.get_players((ip, port).into()).await?;

        assert_eq!(r.len(), 26);

        assert_eq!(
            r.first().unwrap(),
            &A2sPlayer2 {
                index: 0,
                name: "ATon2".to_string(),
                score: 0,
                duration: 9891.6045,
            }
        );

        assert_eq!(
            r.last().unwrap(),
            &A2sPlayer2 {
                index: 0,
                name: "astropuck".to_string(),
                score: 0,
                duration: 39.101303,
            }
        );

        Ok(())
    }

    /*
        #[tokio::test]
        async fn test_ping() -> Result<(), Error> {
            let socket = UdpSocket::bind("0.0.0.0:0").await?;

            let socket = Box::new(MySocketRec {
                socket,
                nb_packet: 0,
                label: "ping".to_string(),
            });
            let mut f = Fetcher { socket };
            let ip: std::net::IpAddr = "100.90.17.102".parse()?;
            let port: u16 = 27165;
            let r = f.ping((ip, port).into()).await?;

            dbg!(&r);
            todo!();

            // Ok(())
        }
    */

    #[tokio::test]
    async fn test_rules() -> Result<(), Error> {
        let socket = UdpSocket::bind("0.0.0.0:0").await?;

        let socket = Box::new(MySocketReplay {
            socket,
            nb_packet: 0,
            label: "rules".to_string(),
        });
        let mut f = Fetcher { socket };
        let ip: std::net::IpAddr = "92.118.18.108".parse()?;
        let port: u16 = 27165;
        let _r = f.get_rules((ip, port).into()).await?;
        // TODO: assert
        Ok(())
    }

    #[tokio::test]
    async fn test_rules2() -> Result<(), Error> {
        let socket = UdpSocket::bind("0.0.0.0:0").await?;

        let socket = Box::new(MySocketReplay {
            socket,
            nb_packet: 0,
            label: "rules2".to_string(),
        });
        let mut f = Fetcher { socket };
        let ip: std::net::IpAddr = "92.118.18.108".parse()?;
        let port: u16 = 27165;
        let _r = f.get_rules((ip, port).into()).await?;
        // TODO: assert
        Ok(())
    }
}
